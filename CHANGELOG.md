[0.1.0]
* Initial version
* Use Ghost 1.5.0

[0.2.0]
* Use Ghost 1.5.2
* Pin knex-migrator package
* New medialinks

[1.0.0]
* Use Ghost 1.6.0
* Unsplash integration (TryGhost/Ghost-Admin#827)
* Added slug & id matching to {{#has}} helper (#8903)
* Added number & index matching to {{#has}} helper (#8902)
* Fixed duplicate subdirs in plaintext (#8882)
* Added 409 UpdateCollisionError for the editor (#8899)
* Removed Casper's gulpfile from npmignore (#8880)
* Added 409 DisabledFeatureError for labs features (#8890)
* Channels: Stored config in res.locals not req (#8884)
* Fix order of multiple upload results (TryGhost/Ghost-Admin#825)

[1.0.1]
* Use Ghost 1.6.1
* Fix missing body classes and broken {{if}} helper (#8910)
* Fix Unsplash when using Night Shift (TryGhost/Ghost-Admin#828)

[1.0.2]
* Use Ghost 1.6.2
* Update ghost-cli to 1.1.1
* Improve layout of team invites list at mobile sizes (TryGhost/Ghost-Admin#813)
* Fix positioning of direction arrows in subscribers table (TryGhost/Ghost-Admin#824)
* Fix legibility of contributor names in Night Shift
* Improve legibility of theme uploader text in Night Shift (TryGhost/Ghost-Admin#823)
* Fix missing Unsplash icons in uploaders when using config override (TryGhost/Ghost-Admin#829)

[1.1.0]
* Use Ghost 1.7.0
* Added any & all matching to {{#has}} helper (TryGhost/Ghost#8916)
* Fix unexpected file dialog when pressing on user screen (TryGhost/Ghost-Admin#833)
* Fix save button showing "Retry" when user slug field loses focus (TryGhost/Ghost-Admin#834)
* Full changelog - https://gist.github.com/kevinansfield/fab35756301308aed6e8f03b39549ce6

[1.1.1]
* Use Ghost 1.7.1
* Fixed internal tags being used as primary tags (TryGhost/Ghost#8934)
* Fixed field focus when adding navigation items (TryGhost/Ghost-Admin#835)
* Full changelog - https://gist.github.com/kevinansfield/929a2d4f95d4e5e1d53e1cae1ee579a9

[1.2.0]
* Use Ghost 1.8.0
* Includes new Casper 2.1.0 that has changes to <blockquote> and <h5> styling
* Fixed post saving indicator only indicating autosaves (TryGhost/Ghost-Admin#842)
* Fixed redirects loop if admin url does not equal blog url (TryGhost/Ghost#8950)
* Fix #tags added via PSM having "public" visibility (TryGhost/Ghost-Admin#841)
* Fix double-scrollbar in code injection editor (TryGhost/Ghost-Admin#838)
* Downgraded errors to warnings for incorrect {{img_url}} usage
* Full changelog - https://gist.github.com/kevinansfield/9676f904271ad4c69dcca07b47ff13cf

[1.2.1]
* Use Ghost 1.8.1
* Fixed "active" state of apps on apps index screen (TryGhost/Ghost-Admin#843)
* https://gist.github.com/kevinansfield/233eb57c5895f7601adba692c68bad94

[1.2.2]
* Use Ghost 1.8.2
* Fixed only save imported post ids if amp field is empty (TryGhost/Ghost#8967)

[1.2.3]
* Use Ghost 1.8.3

[1.2.4]
* Use Ghost 1.8.4

[1.2.5]
* Use Ghost 1.8.5
* Fixed team page not showing profile image and last seen (TryGhost/Ghost-Admin#857)
* Fixed invalid image URLs not being cached and causing timeouts (TryGhost/Ghost#8986)
* Fixed private blogging leaking post information (TryGhost/Ghost#8999)
* Fixed custom redirects with query/search params (TryGhost/Ghost#8998)
* Improved visibility of unchecked checkboxes (TryGhost/Ghost-Admin#851)
* https://gist.github.com/AileenCGN/03a1ab0805bdf6f6f5220d99f53bf868

[1.2.6]
* Use Ghost 1.8.6
* Fixed doubled query params for url/admin-url redirection
* Fixed public api access on custom domain
* Changed showing update notifications for minor/major only (TryGhost/Ghost#9009)
* Fixed disqus comment id when exporting/importing 1.x content
* https://gist.github.com/AileenCGN/307346d2c75553f71d6b74ce1c097e58

[1.2.7]
* Use Ghost 1.8.7
* Fixed "scheduled at" datepicker not showing correct month when opening (TryGhost/Ghost-Admin#860)
* Fixed api url for the ghost sdk (TryGhost/Ghost#9013)
* https://gist.github.com/kirrg001/4dadb2a2d7ba71f768e34d2bb42ef650

[1.3.0]
* Use Ghost 1.9.0
* Upload/Download redirects UI - Kevin Ansfield
* Allow Upload/Download of redirects.json (TryGhost/Ghost#9029) - Katharina Irrgang
* Fixed unreadable Unsplash search text in Night Shift mode (TryGhost/Ghost-Admin#866) - Kevin Ansfield
* Fixed missing Unsplash icons when API doesn't return an unsplash setting (TryGhost/Ghost-Admin#865) - Kevin Ansfield
* Linked tags story count to filtered stories list (TryGhost/Ghost-Admin#863) - Kevin Ansfield
* Updated Ghost-Admin: Enabled Unsplash integration by default - kirrg001
* Enabled Unsplash integration by default (TryGhost/Ghost-Admin#862) - Kevin Ansfield
* Improved log output for welcome email error (TryGhost/Ghost#9016) - Katharina Irrgang
* Unsplash by default - kirrg001
* Removed private configuration endpoint - kirrg001

[1.3.1]
* Use Ghost 1.9.1
* Backup redirects.json file before overwriting (TryGhost/Ghost#9051)
* Mellowed the bright white info boxes in night shift mode (TryGhost/Ghost-Admin#869)
* Fixed persistent upgrade notifications showing for the currently installed version (TryGhost/Ghost#9048)
* Fixed incorrect publish menu date/time input sizes in IE11 (TryGhost/Ghost-Admin#853)
* Fixed blog icon display in admin menu when using a storage adapter (TryGhost/Ghost-Admin#831)
* Fixed incorrect Home/End key behaviour on Windows (TryGhost/Ghost-Admin#870)
* Fixed missing "file too large" text for failed import uploads (TryGhost/Ghost-Admin#867)
* Fixed redirects upload for various browsers/systems (TryGhost/Ghost#9042)
* https://gist.github.com/kevinansfield/d636823cd2670fed033b46160cc6e6ad

[1.4.0]
* Use Ghost 1.10.0
* Removed certain fields from public user response (TryGhost/Ghost#9069)
* Removed public API endpoint to fetch users by email address (TryGhost/Ghost#9059)
* User is not allowed to add/modify certain fields (TryGhost/Ghost#9053)
* Fixed incorrect autosave of published posts when leaving editor (TryGhost/Ghost-Admin#879)
* Fixed admin search not handling certain characters (TryGhost/Ghost-Admin#877)
* Fixed editor font in MS Edge (TryGhost/Ghost-Admin#876)
* Fixed broken "retry" link when an Unsplash API request failed (TryGhost/Ghost-Admin#875)
* Fixed visibility of spellcheck errors in Night Shift mode (TryGhost/Ghost-Admin#874)
* Fixed missing cache invalidation header when uploading redirects (TryGhost/Ghost#9071)
* Fixed various iOS issues (TryGhost/Ghost-Admin#868)
* Fixed author role permission to prevent Authors changing post author (TryGhost/Ghost#9067)
* Fixed API returning roles on the public user resource (TryGhost/Ghost#9039)

[1.5.0]
* Use Ghost 1.11.0
* for new posts, text which looks like a valid domain (eg. example.com) will no longer be auto-linked
* when editing old posts, any links on previously auto-linked domains will be removed
* if you want to link example.com it will be necessary to use full markdown syntax, eg. [example.com](https://example.com)
* https://gist.github.com/AileenCGN/a62a3748ffd4dd1c121588e6e1c7c74c

[1.5.1]
* Use Ghost 1.11.1
* for new posts, text which looks like a valid domain (eg. example.com) will no longer be auto-linked
* when editing old posts, any links on previously auto-linked domains will be removed
* if you want to link example.com it will be necessary to use full markdown syntax, eg. [example.com](https://example.com)
* Fixed being able to store invalid date formats (TryGhost/Ghost#9090)
* https://gist.github.com/AileenCGN/a62a3748ffd4dd1c121588e6e1c7c74c

[1.6.0]
* Use Ghost 1.12.0
* Fixed author helper not returning the correct url (TryGhost/Ghost#9102)
* Private RSS feed (TryGhost/Ghost#9088)
* Added confirmation dialog when leaving settings screen with unsaved changes (TryGhost/Ghost-Admin#871)
* https://gist.github.com/AileenCGN/c7c2ec8f51a2130016769c6492264954

[1.6.1]
* Use Ghost 1.12.1
* Version bump to 1.12.1
* Updated Ghost-Admin to 1.12.1
* Permissions: minor refactors (TryGhost/Ghost#9104)
* https://gist.github.com/kirrg001/aef44a1ab2c11e057c7c97354d9ba346

[1.7.0]
* Use Ghost 1.13.0
* Custom post templates (TryGhost/Ghost#9073)
* Support filtering based on primary_tag (TryGhost/Ghost#9124)
* https://gist.github.com/kirrg001/6292e748c2f35ef2cf4674d8368465f0

[1.8.0]
* Use Ghost 1.14.0
* Prev & next post filtering, with primary tag support (TryGhost/Ghost#9141)
* Improved accessibility markup in default navigation.hbs partial (TryGhost/Ghost#9137)
* HTML page error: correct templateData (TryGhost/Ghost#9144)
* Fixed "Post URL matches" text displaying when no template match is found (TryGhost/Ghost-Admin#896)
* Fixed showing "theme missing" error incorrectly (TryGhost/Ghost#9129)
* https://gist.github.com/kevinansfield/357a35d927dee5579e980162e685b3a6

[1.8.1]
* Use Ghost 1.14.1

[1.9.0]
* Use Ghost 1.15.0
* Support prev/next filtering by author (TryGhost/Ghost#9149)
* Increase minimum password length to 10 characters (TryGhost/Ghost#9152)
* Improved importer logic for password in users (TryGhost/Ghost#9161)
* Fixed anchor links converting to absolute URLs in AMP pages (TryGhost/Ghost#9143)

[1.9.1]
* Use Ghost 1.15.1
* Prevents xmlrpc pings happening on import (TryGhost/Ghost#9165)

[1.10.0]
* Use Ghost 1.16.0
* Improved password validation rules (TryGhost/Ghost#9171)
* Fixed error when theme folder is a number (TryGhost/Ghost#9184)

[1.10.1]
* Use Ghost 1.16.1
* Update Ghost-CLI to 1.2.0
* Bump dependencies
* New tags input, drop selectize & jquery-ui deps (TryGhost/Ghost-Admin#892)
* Moved apps to /services/ & moved individual tests (TryGhost/Ghost#9187)
* Match service/controller import to ember-modules-codemod style for consistency

[1.10.2]
* Use Ghost 1.16.2
* Fixed "Someone else is editing" errors showing when no-one else is editing (TryGhost/Ghost-Admin#901)
* Added confirmation dialogs when leaving screens with unsaved changes (TryGhost/Ghost-Admin#891)

[1.11.0]
* Use Ghost 1.17.0
* New {{reading_time}} theme helper (TryGhost/Ghost#9217)
* Increased maximum tag description length to 500 (TryGhost/Ghost-Admin#904)
* Fixed password fields not being cleared when leaving team/user page (TryGhost/Ghost-Admin#900)
* Fixed custom_excerpt not being used in RSS feeds (TryGhost/Ghost#9219)

[1.11.1]
* Use Ghost 1.17.1
* Increased allowed lengths of tag names/slugs and user names (TryGhost/Ghost-Admin#905)
* Improve admin styles on mobile (TryGhost/Ghost-Admin#903)
* Fixed error for password authentication with Bearer Token (TryGhost/Ghost#9227)
* Fix elastic scroll issues in admin on iOS

[1.11.2]
* Use Ghost 1.17.1
* Update Ghost CLI to 1.2.1

[1.12.0]
* Use Ghost 1.17.2
* Update knex-migrator to 3.0.7
* Fixed missing errors on failed import
* Allow any Slack-compatible webhook URLs in Slack app

[1.13.0]
* Use Ghost 1.18.0
* Fixed slugs from exceeding db limit (TryGhost/Ghost#9251)

[1.13.1]
* Fix issue where Ghost will not install since the CLI is out of date

[1.14.0]
* Use Ghost 1.18.2
* Fixed blog icon not shown in nav (TryGhost/Ghost-Admin#918)
* Fixed unresponsive upload buttons
* Fixed time input bugs when leading zero is omitted

[1.14.1]
* Use Ghost 1.18.3
* Fixed "NaNUndefined" when inserting unordered list items (TryGhost/Ghost-Admin#920)
* Fixed editor image uploads (TryGhost/Ghost-Admin#919)

[1.14.2]
* Use Ghost 1.18.4
* Fixed Ctrl/Cmd+S save shortcut not working when tags input has focus (TryGhost/Ghost-Admin#921)

[1.15.0]
* Update Ghost to 1.19.0
* Update Ghost CLI to 1.4.1

[1.15.1]
* Update Ghost to 1.19.2

[1.16.0]
* Enable output of structured data

[1.17.0]
* Update Ghost to 1.20.0
* Theme translations and blog localisation (TryGhost/Ghost-Admin#703)
* Added Zapier app with pre-built zap templates list (TryGhost/Ghost-Admin#943)
* Move "Update available" notification to the About screen (TryGhost/Ghost-Admin#894)
* Fixed date helper timezone bug (TryGhost/Ghost#9382)
* Fixed editor cursor position on iOS when opening keyboard
* ixed missing cursor and selection issues using editor on iOS
* Fixed navigation url inputs when configured URL has special characters (TryGhost/Ghost-Admin#941)
* Fixed Japanese IME popup position in editor (TryGhost/Ghost-Admin#940)
* Fixed list editing regression in 1.19.2

[1.17.1]
* Update Ghost to 1.20.1

[1.17.2]
* Update Ghost to 1.20.2
* Fixes a problem where the blog kept too many database connections open.

[1.17.3]
* Update Ghost to 1.20.3
* Improved warning about authored posts being removed when deleting a user (TryGhost/Ghost-Admin#951)
* Fixed showing old release notifications in the about page

[1.18.0]
* Update Ghost to 1.21.1
* Contributor Role (TryGhost/Ghost-Admin#948)
* Fixed editor autofocus when starting a new post

[1.18.1]
* Update Ghost to 1.21.2
* Update CLI to 1.5.2
* Fixed {{get}} helper's date comparison (TryGhost/Ghost#9454)
* Fixed asset redirects (TryGhost/Ghost#9455)

[1.18.2]
* Update Ghost to 1.21.3
* [Full changelog](https://gist.github.com/AileenCGN/fb8062dd506a53da2327b6f8a30116cb#file-changelog-1-21-3-md)

[1.18.3]
* Update Ghost to 1.21.4
* Improved image counting for the {{reading_time}} helper (TryGhost/Ghost#9366)
* Fixed unreadable error message colour when uploading themes/images (TryGhost/Ghost-Admin#962)
* Fixed image properties to be reset to null after removal (TryGhost/Ghost#9432)
* Fixed broken i18n docs link on general settings screen (TryGhost/Ghost-Admin#958)

[1.18.4]
* Update Ghost to 1.21.5
* Fixed infinite loading on posts screen (TryGhost/Ghost-Admin#967)
* Fixed "Are you sure" modal being shown incorrectly after toggling private mode (TryGhost/Ghost-Admin#964)
* Fixed need to click Night Shift toggle 3 times in certain circumstances (TryGhost/Ghost-Admin#965)
* Fixed size of the time input fields in publish and post settings menus

[1.18.5]
* Update Ghost to 1.21.6

[1.18.6]
* Update Ghost to 1.21.7
* Fixed updated_at not being updated (TryGhost/Ghost#9532)
* Fixed misspelled schema.org type for WebSite (TryGhost/Ghost#9526)
* Added more language support to {{reading_time}} helper (TryGhost/Ghost#9509)

[1.19.0]
* Update Ghost to 1.22.0
* Allow multiple authors on posts (TryGhost/Ghost#9426)

[1.19.1]
* Update Ghost to 1.22.1

[1.19.2]
* Update knex-migrator
* Make log directory writable for Ghost CLI

[1.19.3]
* Update Ghost to 1.22.2

[1.19.4]
* Update Ghost to 1.22.3

[1.19.5]
* Update Ghost to 1.22.4

[1.19.6]
* Update Ghost to 1.22.5
* Fixed admin URL not updating when changing tag slug
* Fixed 404 handling in admin

[1.19.7]
* Update Ghost to 1.22.7
* Fixed tag creation via the Post Settings Menu
* Fixed invalid character and 'define' is undefined errors in IE11

[1.19.8]
* Update Ghost to 1.22.8
* Update Ghost CLI to 1.7.3

[1.20.0]
* sparkles Koenig Editor Beta Release - See the [beta announcement](https://forum.ghost.org/t/koenig-editor-beta-release/1284) for full details
* bug Fixed /edit shortcut not working in Safari (TryGhost/Ghost#9637)
* bug Fixed gscan errors not caught for corrupted zips

[1.21.0]
* Update Ghost to 1.23.1
* Update Ghost CLI to 1.8.0

[1.22.0]
* Update Ghost to 1.24.1
* Update Ghost CLI to 1.8.1
* Routing Beta (TryGhost/Ghost#9596)
* Fixed short urls when private blogging is enabled (TryGhost/Ghost#9628)
* Fixed broken scrolling/scroll bars in Code Injection inputs
* Koenig - Drag & drop image card creation + upload
* Koenig - ^superscript^ and ~subscript~ text expansion support
* Fixed urls being /404/ after starting the Ghost server
* Fixed post redirect with query params
* Fixed duplicate entries in log files (ghost-ignition@2.9.3)
* Koenig - Fixed scroll jump when card is selected
* Importer no longer imports the slack hook

[1.22.1]
* Update Ghost to 1.24.2

[1.22.2]
* Update Ghost to 1.24.4

[1.22.3]
* Update Ghost to 1.24.5

[1.22.4]
* Update Ghost to 1.24.6

[1.22.5]
* Update Ghost to 1.24.7

[1.22.6]
* Update Ghost to 1.24.8

[1.22.7]
* Update Ghost to 1.24.9
* Fixed Cmd+Backspace with special chars and Firefox triple-click behaviour
* Fixed clicking certain icons in /-menu not creating cards
* Fixed "Retry" link in embed card error message
* Fixed error when clicking embed icons in /-menu
* Fixed up/down cursor movement between paragraphs in Firefox
* Fixed /-menu not working after the editor is reloaded

[1.23.0]
* Update Ghost to 1.25.3
* Added word count display
* Implement full set of text editing shortcuts
* Allow Ctrl/Cmd+Enter to exit card edit mode
* Fixed Firefox cursor getting stuck on cards when pressing Up
* Fixed required double-click on (+) when cursor is not in same paragraph
* Fixed Safari scrolling to bottom of post when using link input
* Prevent double-clicks to enter markdown card edit mode triggering toolbar buttons
* Hide sidebar when editing (TryGhost/Ghost-Admin#1030)
* Fixed numpad Enter key not behaving like normal Enter key

[1.23.1]
* Update Ghost to 1.25.4
* Koenig - Added rich-text support to captions
* Koenig - Auto-convert URLs pasted on blank lines to embed cards
* Koenig - Support /image {image url} slash menu shortcut
* Koenig - Fixed cards being lost on copy/paste if they immediate followed a list
* Koenig - Fixed duplicate images when dropping image file on an image card
* Koenig - Fixed error when dropping an image before the editor has focus

[1.23.2]
* Update Ghost to 1.25.5

[2.0.0]
* Update Ghost to 2.0.3
* [New editor](https://help.ghost.org/article/29-ghost-editor-overview) - rich-text editing with the familiarity of markdown
* [Dynamic routing](https://docs.ghost.org/v2/docs/dynamic-routing) beta - powerful URL customisation and content separation
* advanced filtering to Dynamic Routing (TryGhost/Ghost#9757)
* ability to upload/reload routes.yaml
* Changed fixture data and default settings (TryGhost/Ghost#9778)
* Importer disallows LTS imports
* Dynamic Routing Beta: Enabled redirect by default when using data key
* Dynamic Routing Beta: Disallow using author resource and author data key name (TryGhost/Ghost#9790)
* Dynamic Routing Beta: Fixed wrong template order for static routes
* Koenig - Fixed pasting of plain text
* Removed demo post (TryGhost/Ghost#9769)
* Removed permalink setting

[2.1.0]
* Update Ghost to 2.1.1
* Added ability to resize and compress images on upload (TryGhost/Ghost#9837)
* Added gallery card to the editor
* Added absolute_urls flag to public api (TryGhost/Ghost#9833)
* Added additional parameters to subscribe_form and input_email helpers (TryGhost/Ghost#9820)
* Fixed count.posts respecting co authors (TryGhost/Ghost#9830)
* Fixed video size in the editor (TryGhost/Ghost-Admin#1039)

[2.1.1]
* Update Ghost to 2.1.2

[2.1.2]
* Update Ghost to 2.1.3

[2.1.3]
* Update Ghost to 2.1.4

[2.2.0]
* Use latest base image

[2.3.0]
* Update Ghost to 2.2.1

[2.3.1]
* Update Ghost to 2.2.2

[2.3.2]
* Update Ghost to 2.3.2

[2.3.3]
* Update Ghost to 2.3.3

[2.4.0]
* Update Ghost to 2.3.0
* [Full changelog](https://gist.github.com/rishabhgrg/d2c9dd11931bd6effd2e38393b7d4ba0)
* Added edit webhook route to v2 Admin API (TryGhost/Ghost#10033) - Kevin Ansfield
* Added ability to delete integrations (TryGhost/Ghost-Admin#1057) - Fabien O'Carroll
* Added edit webhook modal (TryGhost/Ghost-Admin#1056) - Kevin Ansfield
* Updated webhook request header and last triggered error mssg (TryGhost/Ghost#10035) - Rishabh Garg
* Fixed read posts/pages for v2 - kirrg001
* Improved api key clipboard copy behaviour - Kevin Ansfield
* Fixed a link typo in CONTRIBUTING.md (TryGhost/Ghost#10034) - Andrey Ozornin
* Added new site.changed event and webhook trigger service (TryGhost/Ghost#10019) - Rishabh Garg
* Changed "new-webhook" modal to generic "webhook-form" modal - Kevin Ansfield
* Fixed serialisation of belongsTo relationships - Kevin Ansfield
* Added HTTP BREAD for integrations resource (TryGhost/Ghost#9985) - Fabien O'Carroll

[2.5.0]
* Update Ghost to 2.4.0
* Fixed missing filename when exporting subscribers csv
* Fixed pagination for subscribers
* Fixed cardWidth being lost on 2.0 imports (#10068)
* Corrected 'Content-Length' header by using Buffer.byteLength

[2.6.0]
* Update Ghost to 2.5.0

[2.7.0]
* Update Ghost to 2.6.0
* Highlight new integrations directory (TryGhost/Ghost-Admin#1070)
* Fixed "last seen" info for users (#10141)
* Fixed order being ignored in routes.yaml file (#10146)
* Fixed image optimisation where "optimised" image ended up larger than the uploaded image
* Fixed administrators not being able to edit webhooks

[2.7.1]
* Update Ghost to 2.6.2
* Added "Recently updated" sorting option to Stories screen (TryGhost/Ghost-Admin#1023)
* Fixed missing 404 for unknown API routes (#10070)
* Protected Ghost blog against invalid uploaded routes.yaml (#10100)

[2.8.0]
* Update Ghost to 2.7.1
* Added new theme directory links to Ghost Admin (TryGhost/Ghost-Admin#1078)
* auto redirect for extra data queries on post and page resources (#9828)
* reference warning from importer report if post is a draft (#10169)
* Fixed edit permission of the common article by multiple authors (#10214)
* Fixed the link for Markdown guide documentation. (TryGhost/Ghost-Admin#1076)
* Fixed site changed webhook not triggered for scheduled posts

[2.9.0]
* Update Ghost to 2.8.0
* Added drag-n-drop re-ordering of images within gallery cards (TryGhost/Ghost-Admin#1073)
* Added CORS support to the v2 Content API (#10257)
* Fixed all known filter limitations (#10159)
* Fixed ability to submit invite form with a missing role (TryGhost/Ghost-Admin#1080)

[2.10.0]
* Update Ghost to 2.9.0
* Added support for dynamic image resizing (#10184)
* Added size attribute support to {{img_url}} helper (#10182)
* Added drag-and-drop card re-ordering in the editor (TryGhost/Ghost-Admin#1085)
* Added auto-scroll when dragging cards or gallery images (TryGhost/Ghost-Admin#1083)
* Fixed URL is not a constructor for Node v6 (#10289)
* Removed duplicate shortcut icon link on amp pages (#10254)
* Fixed multiple authors on amp pages (#10253)
* Fixed get helper when API v0.1 is disabled (#10270)

[2.10.1]
* Update Ghost to 2.9.1

[2.11.0]
* Update Ghost to 2.10.0
* New & improved v2 Content API
* Aliased {{@blog}} as {{@site}} in the theme API
* Use unoptimised image when possible for dynamic images (#10314)
* Fixed responsive images for gifs & svgs (#10315)

[2.11.1]
* Update Ghost to 2.10.1
* Removed brute force middleware form content api

[2.11.2]
* Update Ghost to 2.10.2

[2.12.0]
* Update Ghost to 2.11.1
* Upgrading Casper to 2.9.0 which uses Content API v2 as default Ghost API
* Fixed custom redirects to forward hashes correctly (#10319)
* Fixed 500 error for content with no collection
* Fixed uncaught exceptions from image fetches
* Fixed responsive images for .icos

[2.13.0]
* Update Ghost to 2.12.0

[2.14.0]
* Update Ghost to 2.13.1
* Fixed crash on startup when no active theme (#10426)
* Fixed error for missing sharp install (#10423)
* Fixed drag-n-drop card reordering interfering with caption and markdown/html card text selection
* Fixed 404 for locked or suspended users

[2.14.1]
* Update Ghost to 2.13.2

[2.15.0]
* Update Ghost to 2.14.0

[2.15.1]
* Allow 'url' in config file to be different from 'admin'

[2.15.2]
* Update Ghost to 2.14.2

[2.15.3]
* Update Ghost to 2.14.3

[2.16.0]
* Update Ghost to 2.15.0
* Fixed filtering by primary_tag or primary_author in routes.yaml
* Fixed night mode when using API v2 (#10499)
* Added posibility to accept html as an input source for post
* Casper (the default theme) has been updated to 2.9.3
* Fixed bug with embeds overflowing content width

[2.16.1]
* Update Ghost to 2.16.1
* Added post, page, and tag related webhook trigger events
* Added Admin API v2 stable endpoints for posts, pages, and images
* Separated post and page list screens (TryGhost/Ghost-Admin#1101)
* Fixed markdown text expansions sometimes resulting in sticky formatting
* Fixed Code Injection inputs not being clickable
* Fixed empty subscribers webhooks payload

[2.16.2]
* Update Ghost to 2.16.2
* Fixed theme upload

[2.16.3]
* Update Ghost to 2.16.3
* 500 error when an unknown field was passed in the fields API param
* fields param having no effect for /authors/:id and /tags/:id endpoints
* 404 when trying to update codeinjection_* settings in Admin API v2
* collection naming when specifying keys in a route's data property (see #10434) (#10559)
* "value" properties sometimes being missing in settings endpoint in Admin API v2
* excerpt property being missing if plaintext is NULL or "" in posts endpoint
* plaintext field not being cleared when post body is removed
* "false" not being recognised in absolute attribute value of {{url absolute="false"}} helper (#10556)
* {{excerpt}} helper sometimes displaying null
* AMP pages being empty
* {{url}} output when using {{#next_post}}/{{#prev_post}}

[2.16.4]
* Update Ghost to 2.16.4
* Fixed private blogging getting enabled on restart after saving any setting (#10576)
* Fixed v0.1 API username/password authentication
* Fixed error when creating subscribers via the admin area
* Fixed email address not being returned in user responses from v2 Admin API using api key authentication

[2.17.0]
* Update Ghost to 2.18.2
* Fixed null displayed on empty post content (#10617) - Eol
* Fixed infinite scroll of posts/pages screens not working in Safari - Kevin Ansfield
* Fixed unnecessary "Are you sure you want to leave?" modals - Kevin Ansfield
* Fixed difficult to cancel webhook modal - Kevin Ansfield

[2.18.0]
* Update Ghost to 2.19.1
* Fixed error after logging in from a nested admin URL
* Fixed missing error notification when attempting to activate an invalid theme
* Fixed AMP output when there is a trailing '$'

[2.19.0]
* Update Ghost to 2.19.2

[2.19.1]
* Update Ghost to 2.19.3

[2.19.2]
* Update Ghost to 2.19.4

[2.20.0]
* Update Ghost to 2.20.1

[2.21.0]
* Update Ghost to 2.21.0
* Fixed redirect for pages causing 404 - Nazar Gargol
* Made notifications dismissible per user - Nazar Gargol

[2.21.1]
* Update Ghost to 2.21.1

[2.22.0]
* Update Ghost to 2.22.0

[2.22.1]
* Update Ghost to 2.22.1
* Update casper theme

[2.22.2]
* Update Ghost to 2.22.3

[2.23.0]
* Update Ghost to 2.23.0
* Upgraded our Zapier integration to use API Keys and allow post creation

[2.23.1]
* Update Ghost to 2.23.1

[2.23.2]
* Update Ghost to 2.23.3

[2.23.3]
* Update Ghost to 2.23.4

[2.24.0]
* Update Ghost to 2.25.1
* Use manifest v2

[2.24.1]
* Update Ghost to 2.25.3

[2.24.2]
* Update Ghost to 2.25.4

[2.24.3]
* Update Ghost to 2.25.6
* Fixed infinite redirect for amp when disabled - Hannah Wolfe
* Fixed unexpected image positions when re-ordering gallery images - Kevin Ansfield
* Updated theme activation API to experimental - Hannah Wolfe
* Added ability to drag images in and out of galleries - Kevin Ansfield
* Fixed theme upload error when overriding existing theme - Rish

[2.24.4]
* Update Ghost to 2.25.8

[2.24.5]
* Update Ghost to 2.25.9

[2.25.0]
* Update Ghost to 2.26.0
* Added url value to the Content API /settings/ endpoint (#10946) - Kevin Ansfield
* Added ability to edit alt text for image cards - Kevin Ansfield
* Fixed img_url helper when using image sizes with relative path (#10964) - Fabien O'Carroll
* Casper (the default theme) has been upgraded to 2.10.5

[2.26.0]
* Update Ghost to 2.27.0

[2.28.0]
* Update Ghost to 2.28.0

[2.29.0]
* Fix DB migration issues

[2.29.1]
* Update Ghost to 2.28.1
* Fixed an issue with imports created prior to 2.28.0 where pages became posts when importing

[2.100.0]
* Update Ghost to 2.28.1
* Fixed an issue with imports created prior to 2.28.0 where pages became posts when importing

[2.100.1]
* Update Ghost to 2.29.1

[2.101.0]
* Update Ghost to 2.30.2
* **Themes will require updates to be compatible with the new bookmark card!**
* Added bookmark card and integrated it as fallback for unknown embeds (TryGhost/Ghost-Admin#1293) - Rishabh Garg
* Added "What's new" indicator and modal to highlight recent updates (TryGhost/Ghost-Admin#1292) - Kevin Ansfield
* Updated tags screen design and usability (TryGhost/Ghost-Admin#1283) - Rishabh Garg
* Fixed lack of space in excerpt generated from paragraphs - Nazar Gargol
* Casper (the default theme) has been upgraded to 2.11.0:
* Added CSS for bookmark card (TryGhost/Casper#607) - Rishabh Garg
* Fixed display of markdown, html, and code card editors - Kevin Ansfield

[2.102.0]
* Update Ghost to 2.31.0

[2.103.0]
* Update Ghost to 2.31.1

[2.104.0]
* Update Ghost to 2.37.0
* **Note**: intermediate releases are skipped intentionally because Ghost had a [migration bug](https://github.com/TryGhost/Ghost/releases/tag/2.35.0)

[3.0.0]
* Update Ghost to 3.0.2
* [Ghost 3](https://ghost.org/blog/3-0/)
* Bookmark Cards
* Improved WordPress Migration Plugin
* Responsive Image Galleries & Images
* Members & Subscriptions option
* Stripe: Payment Integration
* Default Theme Improvement

[3.0.1]
* Update Ghost to 3.0.3

[3.100.0]
* Update Ghostt to 3.1.0

[3.100.1]
* Udate Ghost to 3.1.1

[3.101.0]
* Update Ghost to 3.2.0

[3.102.0]
* Update Ghost to 3.3.0

[3.103.0]
* Update Ghost to 3.4.0

[3.104.0]
* Update Ghost to 3.5.0

[3.104.1]
* Update Ghost to 3.5.1

[3.104.2]
* Update Ghost to 3.5.2

[3.105.0]
* Update Ghost to 3.6.0

[3.106.0]
* Update Ghost to 3.7.0

[3.107.0]
* Update Ghost to 3.8.0

[3.108.0]
* Update Ghost to 3.9.0

[3.109.0]
* Update Ghost to 3.10.0

[3.110.0]
* Update Ghost to 3.11.0

[3.111.0]
* Update Ghost to 3.12.0

[3.112.0]
* Update Ghost to 3.12.1

[3.113.0]
* Update Ghost to 3.13.1

[3.113.1]
* Update Ghost to 3.13.3

[3.113.2]
* Update Ghost to 3.13.4

[3.114.0]
* Update Ghost to 3.14.0
* Use latest base image 2.0

[3.115.0]
* Update Ghost to 3.15.3

[3.116.0]
* Update Ghost to 3.16.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.16.0)
* Added success indicator for members magic links - Hannah Wolfe
* Updated @tryghost/zip to fix dotfile support - Hannah Wolfe

[3.116.1]
* Update Ghost to 3.16.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.16.1)
* Fixed logic error in navigation for isSecondary - Hannah Wolfe
* Fixed permission for "Administrator" to be able to edit post visibility - Naz Gargol
* Fixed structured data issue for publisher logo - Aileen Nowak

[3.117.0]
* Update Ghost to 3.17.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.17.0)
* Added Admin API endpoint for basic member stats (#11840) - Kevin Ansfield
* Removed incorrect a11y roles from navigation template (#11833) - Marco Zehe

[3.118.0]
* Update Ghost to 3.18.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.18.0)
* Improved security of oembed endpoint by restricting access to sites on private IP blocks - Kevin Ansfield
* Improved performance of members admin screens - Kevin Ansfield
* Added ?search= param to Admin API members endpoint (#11854) - Kevin Ansfield
* Fixed iframe script for AMP not injected - Aileen Nowak
* Fixed various Close buttons throughout the UI. (#1585) - Marco Zehe
* Fixed various small accessibility problems in the admin screen (#1583) - Marco Zehe
* Fixed email preview not visible to Editors - Rish
* Fixed test email permissions for non-owner roles - Rish

[3.118.1]
* Update Ghost to 3.18.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.18.1)

[3.119.0]
* Update Ghost to 3.19.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.19.0)

[3.119.1]
* Update Ghost to 3.19.2
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.19.2)
* Fixed issue with member signin/signup URLs

[3.120.0]
* Update Ghost to 3.20.2
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.20.0)
* Added INR currency support (#11911) - Hannah Wolfe
* Added all/free/paid filter to members admin screen (TryGhost/Ghost-Admin#1600) - Kevin Ansfield
* Added /members/validate ednpoint to Admin API - Nazar Gargol

[3.120.1]
* Update Ghost to 3.20.3
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.20.3)
* Do not backup log files

[3.121.0]
* Update Ghost to 3.21.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.20.1)
* Added sizes attribute to in-content <img> elements - Kevin Ansfield
* Added tags sidebar navigation for editors - Rishabh Garg

[3.122.0]
* Update Ghost to 3.21.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.21.1)
* Fixed potentially squashed images in image cards - Kevin Ansfield

[3.123.0]
* Update Ghost to 3.22.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.22.1)
* Switched on Stripe Connect by default - Fabien O'Carroll
* Fixed global code injection not being output - Kevin Ansfield
* Fixed settings cache being out of sync after migrations (#11987) - Kevin Ansfield

[3.123.1]
* Update Ghost to 3.22.2
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.22.2)
* Updated access to be true by default in v3 API - Hannah Wolfe
* Emitted all settings events on reinit of cache (#12012) - Fabien O'Carroll
* Hardened members subscription migration against missing data (#12009) - Fabien O'Carroll

[3.124.0]
* Update Ghost to 3.24.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.24.0)
* We skipped 3.23.x because of [migration bug](https://github.com/TryGhost/Ghost/issues/12026)

[3.125.0]
* Update Ghost to 3.25.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.25.0)
* Added UI for setting tag metadata (TryGhost/Ghost-Admin#1632) - Fabien O'Carroll
* Fixed Stripe webhooks for subdirectory setups - Fabien O'Carroll

[3.126.0]
* Update Ghost to 3.26.1
* Update ghost-cli to 1.14.1
* Ensured webhooks are created once (#12075) - Fabien O'Carroll

[3.126.1]
* Update Ghost to 3.27.0
* Add env file support to override app configs

[3.127.0]
* Update Ghost to 3.28.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.28.0)
* Fixed incorrect scheduled date in toast (TryGhost/Ghost-Admin#1663) - Roshan Dash
* Fixed installation on windows #10890 (#12096) - CY Lim
* Fixed incorrect member gating on custom static page routes - Rishabh Garg
* Fixed 500 error in webhooks API when modifying non-existing webhooks - Nazar Gargol
* Fixed missing autosave indicator regression in editor - Kevin Ansfield
* Casper (the default theme) has been upgraded to 3.1.0.

[3.128.0]
* Update Ghost to 3.29.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.29.0)
* No user-visible changes in this release

[3.128.1]
* Update Ghost to 3.29.2
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.29.2)
* bug Fixed failing migration from <2.34 to 3.29 - Kevin Ansfield
* bug Fixed published time and modified time for structured data (#12085) - Roshan Dash
* bug Fixed meta data when using tag data in collection (#12137) - Fabien 'egg' O'Carroll
* bug Removed [http://url/] output in member email preview text - Kevin Ansfield
* bug Fixed apostrophes not displaying correctly in Outlook for member emails - Kevin Ansfield

[3.129.0]
* Update Ghost to 3.30.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.30.1)
* Fixed card spacing and caption styling for member emails in Outlook - Kevin Ansfield
* Allowed Admin users to impersonate members - Rishabh Garg
* Fixed bookmark card author/publisher spacing in member emails (#12134) - Jeremy Davidson
* Fixed emails appearing very wide in Outlook and improved email image sizes - Kevin Ansfield

[3.129.1]
* Update Ghost to 3.30.2
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.30.2)

[3.130.0]
* Update Ghost to 3.31.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.31.0)
* Added subscription cancellation button to member page (TryGhost/Ghost-Admin#1683) - Rishabh Garg
* Added admin endpoint for editing member subscription (#12145) - Rishabh Garg
* Fixed response for members API update endpoint - Rishabh Garg

[3.130.1]
* Update Ghost to 3.31.2
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.31.2)
* Fixed bookmark card display in member emails when using Gmail+Chrome - Kevin Ansfield

[3.130.2]
* Update Ghost to 3.31.3
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.31.3)
* Fixed Tags API v2 to return posts count - Nazar Gargol

[3.130.3]
* Update Ghost to 3.31.5
* Fixed incorrect status used for trial subscription query - Kevin Ansfield

[3.131.0]
* Update Ghost to 3.32.1
* Added new support and reply-to address for members - Rishabh Garg

[3.131.1]
* Update Ghost to 3.32.2
* Fixed author/tag management in post settings menu - Kevin Ansfield

[3.132.0]
* Update Ghost to 3.33.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.33.0)
* Added Location header to API's POST request responses (#12186) - Nazar Gargol
* Fixed table constraint error when updating member's email with an already existing email (#12178) - Talha
* Fixed error caused by accepting invitation with existing email (#12172) - Talha

[3.133.0]
* Update Ghost to 3.34.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.34.0)
* Added support for Stripe promo codes in config (#12149) - Kristian Freeman
* Fixed missing start date in members chart (TryGhost/Ghost-Admin#1644) - Umur Dinçer
* Fixed Ctrl/Cmd+S triggering browser save when tags or authors input has focus (TryGhost/Ghost-Admin#1707) - Kukhyeon Heo

[3.133.1]
* Update Ghost to 3.34.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.34.1)
* Fixed email verification mails not sent - Rishabh Garg

[3.134.0]
* Update Ghost to 3.35.3
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.35.3)
* Update Members to sync email changes with Stripe (#12236) - Fabien 'egg' O'Carroll
* Added support for data-members-name in themes (#12191) - Léo Bourrel
* Fixed CSV import json-schema email validation (#12239) - Fabien 'egg' O'Carroll
* Fixed tag metadata fields not being populated when editing in admin (#1714) - Fabien 'egg' O'Carroll
* Fixed email verification mails not sent - Rish
* Fixed social settings not saved on keyboard shortcut (#1633) - Rishabh Garg
* Fixed newsletters emails having no subject - Kevin Ansfield
* Fixed email showing as success when an email batch fails to send - Kevin Ansfield
* Fixed email not showing as failed if error occurs when preparing email - Kevin Ansfield
* Fixed newsletters being sent to Stripe customer emails in place of member emails - Kevin Ansfield
* Fixed "Validation failed for posts[0]" error when saving a post - Naz

[3.134.1]
* Update Ghost to 3.35.4
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.35.4)
* Fixed broken embeds cards when pasting links to Wordpress sites (#12262) - Kevin Ansfield
* Fixed email card replacements showing raw replacement text in emails - Kevin Ansfield
* Fixed meta attributes calculation on post preview - Nazar Gargol

[3.134.2]
* Update Ghost to 3.35.5
* Fixed scheduled post emails pointing at /404/ for the "view online" link - Kevin Ansfield

[3.135.0]
* Update Ghost to 3.36.0
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.36.0)
* Added Ctrl/Cmd+P shortcut in editor to open preview in new tab - Kevin Ansfield
* Fixed special chars in single use token (#12290) - Rishabh Garg
* Fixed width/height attributes in gallery cards not matching resized images - Kevin Ansfield

[3.136.0]
* Update Ghost to 3.37.1
* Fixed snippet icon showing in card toolbar for authors/contributors
* Enabled Portal (#12317) - Rishabh Garg
* Added snippets feature to editor - Kevin Ansfield
* Added support for Node 14 - Austin Burdine
* Fixed server error for repeated order query parameter - Nazar Gargol
* Fixed post resource filtering by posts_meta table fields (#12307) - Nazar Gargol
* Added past_due and unpaid subscriptions for members (#12301) - Rishabh Garg

[3.137.0]
* Update Ghost to 3.38.1
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.38.1)
* Added newsletter design settings (#12352) - Peter Zimon
* Allowed sending newsletter to free members only (TryGhost/Ghost-Admin#1751) - Rishabh Garg
* Added .yaml format support in redirects configuration (#12187) - Kukhyeon Heo
* Fixed bookmark card caption formatting showing as visible html - Kevin Ansfield

[3.137.1]
* Update Ghost to 3.38.2
* [Full changes](https://github.com/TryGhost/Ghost/releases/tag/3.38.2)
* Added missing Bluebird require in v2 API - Daniel Lockyer

[3.137.2]
* Update Ghost to 3.38.3
* Fixed authors failing to publish post with newsletters - Rishabh Garg
* Fixed editor unable to handle non-latin IME languages - Daniel Lockyer

[3.138.0]
* Update Ghost to 3.39.0
* Added email open rate to posts list in admin (TryGhost/Ghost-Admin#1772) - Sanne de Vries
* Added cancellation reason to Members UI (TryGhost/Ghost-Admin#1782) - Fabien 'egg' O'Carroll
* Supported custom redirects for member signup (TryGhost/Ghost-Admin#1768) - Peter Zimon
* Added new portal links for plan checkout (TryGhost/Ghost-Admin#1770) - Rishabh Garg
* Added staff personal tokens - Thibaut Patel
* Fixed infinite spinner when admin login attempt fails (TryGhost/Ghost-Admin#1755) - Kukhyeon Heo
* Fixed backwards compatibility for newsletters (#12422) - Fabien 'egg' O'Carroll
* Fixed flat member chart for GMT-X timezones - Scott Cabot

[3.138.1]
* Update Ghost to 3.39.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.39.2)
* Added guard for page.items existing in Mailgun response - Kevin Ansfield
* Fixed copy for open-tracking settings - Sanne de Vries
* Fixed email cards missing an edit icon in the toolbar - Kevin Ansfield

[3.138.2]
* Update Ghost to 3.39.3
* Fixed email design settings not opening - Rishabh Garg

[3.139.0]
* Update Ghost to 3.40.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.40.1)
* Added activity feed to member details screen (TryGhost/Ghost-Admin#1796) - Sanne de Vries
* Added new members CSV importer (TryGhost/Ghost-Admin#1797) - Rishabh Garg
* Added email stats overview to member details page (TryGhost/Ghost-Admin#1795) - Sanne de Vries
* Added open-rate column and ordering to the members list (TryGhost/Ghost-Admin#1790) - Sanne de Vries
* Added email.open_rate order option to posts api (#12439) - Kevin Ansfield
* Fixed comped flag for members - Fabien O'Carroll
* Fixed page preview - Thibaut Patel

[3.139.1]
* Update Ghost to 3.40.2
* Fixed potential "Request entity too large" error when saving members - Kevin Ansfield

[3.139.2]
* Update Ghost to 3.40.3
* Fixed reply-to address not set for newsletters - Rishabh Garg
* Fixed redirects "to" query params forwarding (#12333) - Kukhyeon Heo
* Fixed re-enabling of complimentary subscriptions - Fabien O'Carroll
* Added translation for BookshelfRelationsError - Fabien O'Carroll
* Fixed incorrect locale loading when changing themes - Naz
* Fixed duplicate customers showing for members with multiple subscriptions - Kevin Ansfield

[3.139.3]
* Update Ghost to 3.40.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.40.4)
* Fixed error when deleting a member that has received a newsletter email - Kevin Ansfield

[3.139.4]
* Update Ghost to 3.40.5
* Fixed blank data in member customer tab - Kevin Ansfield

[3.140.0]
* Update Ghost to 3.41.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.0)
* Added new FirstPromoter integration (TryGhost/Admin#1825) - Rishabh Garg
* Updated newsletter design to use accent color - Rishabh Garg
* Split the v3 endpoint from the canary endpoint - Thibaut Patel
* Added multiple use grace period to tokens (#12519) - Fabien 'egg' O'Carroll
* Disabled auto-unsubscribe of members on permanent email failure events - Kevin Ansfield
* Fixed flash of unstyled content appearing when toggling night mode (TryGhost/Admin#1818) - Harsh Maur

[3.140.1]
* Update Ghost to 3.41.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.2)
* Fixed issue opening Stripe Checkout via data-members-plan - Fabien 'egg' O'Carroll

[3.165.0]
* Update Ghost to 3.41.3
* Version of package was bumped to make it clear it is separate from app version
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.3)
* Fixed default urls for checkout session - Rishabh Garg

[3.166.0]
* Update Ghost to 3.41.4
* Update base image to version 3
* Fixed Member model removing labels when unset

[3.166.1]
* Update Ghost to 3.41.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.5)
* Fixed new paid members showing up with status as 'incomplete' - Daniel Lockyer
* Fixed unable to exit code injection settings without save - Rishabh Garg

[3.166.2]
* Update Ghost to 3.41.6
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.6)
* Fixed payment methods not getting attached to subscriptions - Daniel Lockyer
* Fixed EADDRINUSE error handling on NodeJS >=13 - KiraLT

[3.166.3]
* Update Ghost to 3.41.7
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.7)
* Fixed open file handle leak with external jobs - Kevin Ansfield
* Fixed comping members w/ active subscription - Fabien O'Carroll
* Fixed unexpected members-only content appearing in excerpt field (#12670) - Kevin Ansfield

[3.166.4]
* Update Ghost to 3.41.8
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.8)
* Fixed updating member payment details - Fabien O'Carroll

[3.166.5]
* Update Ghost to 3.41.9
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.41.9)
* Fixed cancelling subscriptions when deleting members

[3.167.0]
* Update Ghost to 3.42.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.42.0)

[3.167.1]
* Update Ghost to 3.42.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.42.1)

[3.167.2]
* Update Ghost to 3.42.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/3.42.2)

[3.168.0]
* Update Ghost to 4.0.1
* Members and Portal are enabled by default
* Added support for 113 new currencies (TryGhost/Admin#1853) - Fabien 'egg' O'Carroll
* Added native lazy loading behaviour to content images - Kevin Ansfield
* Added paywall card to mark end of free content preview (#12663) - Kevin Ansfield
* Added ability to make image cards links - Kevin Ansfield
* Added accent color CSS variable to {{ghost_head}} (#12717) - Kevin Ansfield
* Added new boot process - Hannah Wolfe
* Added new members stats endpoints - Rishabh Garg
* Added theme preview mode - Hannah Wolfe
* Added YAML file support for redirects - Kukhyeon Heo
* Flattened members subscriptions data in API - Rishabh Garg
* Dropped apps related tables - Naz G
* Removed labs from settings - Naz G
* Enabled foreign key checks on sqlite3 - Thibaut Patel
* Removed main script that no longer exists - Hannah Wolfe
* Resolved orphaned webhooks - Thibaut Patel
* Renamed the private mode cookie - Thibaut Patel
* Bumped required Ghost-CLI version to 1.16.0 - Daniel Lockyer
* Comes with Casper 4.0
* Fixed maintenance page being excluded from npm build - Austin Burdine

[3.169.0]
* Update Ghost to 4.1.0
* Fixed __GHOST_URL__ appearing in sitemaps (#12787) - Kevin Ansfield
* Fixed unable to delete member (#12784) - Rishabh Garg
* Fixed link contrast in editor with very light/dark accent colors (TryGhost/Admin#1870) - Kevin Ansfield
* Fixed @price template data to work with price helper (#12764) - Fabien 'egg' O'Carroll
* Fixed deleting members to prompt cancellation (TryGhost/Admin#1869) - Fabien 'egg' O'Carroll
* Fixed AMP validation warning from accent color style tag (#12771) - Kevin Ansfield
* Fixed GHOST_URL appearing in generated excerpts - Naz
* Removed the /redirects/json route alias - Thibaut Patel

[3.169.1]
* Update Ghost to 4.1.2
* Fixed 404 errors if navigation URLs are somehow blank - Kevin Ansfield
* Fixed plaintext and excerpt fallbacks having incorrect URLs after domain change (#12811) - Kevin Ansfield
* Fixed limits not allowing contributors to be unsuspended - Hannah Wolfe
* Fixed performance regression introduced in 4.1.0 (#12807) - Kevin Ansfield
* Fixed Admin redirect for newsletter/support email update (#12810) - Rishabh Garg
* Fixed upgrade notification showing post-upgrade - Naz
* Fixed images not having srcset or sizes attributes (#12800) - Kevin Ansfield

[3.170.0]
* Update Ghost to 4.2.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.2.0)
* Fixed incorrect MRR delta calculation (#12823) - Rishabh Garg
* Fixed markdown for linked images being mangled in some cases - Kevin Ansfield
* Fixed incorrect member count on dashboard - Rishabh Garg
* Reduced default exports content for DB APIs (#12818) - Naz

[3.170.1]
* Update Ghost to 4.2.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.2.1)
* Fixed markdown for linked images still being mangled in some cases - Kevin Ansfield

[3.170.2]
* Update Ghost to 4.2.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.2.2)
* Added a way to hide the secret settings once they are set - Thibaut Patel

[3.171.0]
* Update Ghost to 4.3.0
* Added ability to bulk delete members by label or status (TryGhost/Admin#1883) - Kevin Ansfield

[3.171.1]
* Update Ghost to 4.3.1
* Updated stripe prices/products table population - Rishabh Garg

[3.171.2]
* Update Ghost to 4.3.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.3.2)
* Fixed unable to import paid members - Rishabh Garg

[3.171.3]
* Update Ghost to 4.3.3
* Removed unused and insecure preview endpoint - Hannah Wolfe
* Fixed error when using staff access tokens - Daniel Lockyer
* Fixed Ghost 4.3.0 migration that put all sites into "allow free members signup" (#12904) - Kevin Ansfield

[3.172.0]
* Update Ghost to 4.4.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.4.0)
* Added @site.signup_url data property for themes (#12893) - Kevin Ansfield
* Added option to disable member subscriptions (TryGhost/Admin#1925) - Kevin Ansfield
* Restored correct "allow free signup" setting from backup after buggy 4.3 upgrade (#12905) - Kevin Ansfield

[3.173.0]
* Update Ghost to 4.5.0
* Added custom product settings and multiple price support - Fabien 'egg' O'Carroll & Rishabh Garg
* Added support for gating content by member labels and products (#12946) - Kevin Ansfield
* Added label and product options for email recipients (TryGhost/Admin#1947) - Kevin Ansfield
* Added ability to send a newsletter to members with a certain label or product (#12932) - Kevin Ansfield
* Added default newsletter recipients setting (TryGhost/Admin#1946) - Kevin Ansfield
* Updated post settings menu visibility option to support member segments - Kevin Ansfield
* Fixed labels not being saved with member during signup - Kevin Ansfield
* Bumped minimum Node version to 12.22.1 and 14.16.1 - Daniel Lockyer
* Bumped required Ghost-CLI version to 1.17.0 - Daniel Lockyer
* Dropped support for Node 10 - Daniel Lockyer

[3.174.0]
* Update Ghost to 4.6.0
* Simplified management of membership settings
* Reverted ability to set post access level to labels - Kevin Ansfield
* Fixed retry email not timing out on poll - Rishabh Garg
* Fixed creating members linked to stripe customers - Fabien O'Carroll
* Fixed error on saving member with susbcriptions - Rishabh Garg
* Fixed feature images in emails appearing very wide in Outlook - Kevin Ansfield
* Fixed frontmatter-related validation error - Naz G
* Fixed webhook initialization when over limit - Naz G
* Removed 401 error for logged-out member on Portal - Rishabh Garg

[3.174.1]
* Update Ghost to 4.6.4
* Fixed missing Stripe connected check on boot (#12992) - Rishabh Garg
* Fixed portal preview resize when using split front-end/admin URLs (TryGhost/Admin#1980) - Kevin Ansfield
* Fixed error on membership screen when using split front-end/admin URLs - Kevin Ansfield
* Fixed incorrect @price.currency value in themes (#12987) - Rishabh Garg
* Added error message from limit service to theme upload - ceecko
* Fixed incorrect price data in themes (#12985) - Rishabh Garg
* Fixed members list not loading (#12930) - Rishabh Garg

[3.174.2]
* Update Ghost to 4.6.5
* Fixed saving Members with Complimentary plans (#13008) - Fabien 'egg' O'Carroll

[3.174.3]
* Update Ghost to 4.6.6
* Fixed missing complimentary subscription add button - Rishabh

[3.175.0]
* Update Ghost to 4.7.0
* Enabled use of Grammarly extension in the editor - Kevin Ansfield
* Added "labs" setting enabling feature flags - Naz Gargol
* Fixed unable to create new prices on switching Stripe account (#13013) - Rishabh Garg
* Fixed member count not showing in send email confirmation modal - Kevin Ansfield
* Fixed member count in publish menu not matching subscription status - Kevin Ansfield
* Fixed links in email preview not being clickable - Kevin Ansfield
* Fixed logs showing an error when sending an email with no feature image - Kevin Ansfield
* Fixed Enter key not working in send email confirmation modal - Kevin Ansfield
* Fixed blank branding preview on split front-end/admin domain setups - Kevin Ansfield

[3.176.0]
* Update Ghost to 4.8.0
* Added additional newsletter customisation settings (#13074) - Kevin Ansfield
* Added multiple products UI in Portal behind feature flag - Rishabh Garg
* Opened member email settings up to Administrator staff users - Kevin Ansfield
* Moved default focus in editor from body to title for new posts - Kevin Ansfield
* Fixed confusing member count shown in save notification and editor header - Kevin Ansfield
* Fixed sticky "unsaved settings" modal - Daniel Lockyer

[3.176.1]
* Update Ghost to 4.8.3
* Fixed default currency for MRR stats on dashboard - Rishabh Garg
* Fixed error when deleting non-existent snippet - Daniel Lockyer
* Fixed stripe connect modal not closing - Rishabh Garg
* Fixed error when using API to search for tags - Daniel Lockyer
* Fixed incorrect data returned when using API fields - Daniel Lockyer

[3.176.2]
* Update Ghost to 4.8.4
* Fixed error when removing Facebook/Twitter social images on general settings screen - Kevin Ansfield

[3.177.0]
* Update Ghost to 4.9.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.9.0)
* Added detection of conditional partials in themes - Naz
* Added webp image support (#13105) - Matt Hanley
* Added post feature image alt and caption support to editor (TryGhost/Admin#2026) - Kevin Ansfield
* Added feature_image_{alt/caption} to the v4 posts API - Kevin Ansfield
* Added alpha support for segmented email cards - Naz
* Added a "reset all passwords" feature (#13005) - Thibaut Patel
* Adjusted post settings menu design so it can stay open whilst editing - Kevin Ansfield
* Fixed account/dark mode/settings buttons not being accessible on small mobile devices - Kevin Ansfield
* Hid mobile nav bar when in the editor - Kevin Ansfield
* Fixed potential for partial content re-generation in 4.0 migrations (#13120) - Kevin Ansfield
* Fixed incorrectly stored URLs after migration from 3.x to 4.6.1-4.8.4 (#13109) - Kevin Ansfield
* Fixed a 500 error for incorrect fields parameter in API - Naz
* Fixed blank mobile preview for pages in preview modal - Kevin Ansfield
* Fixed validation on theme install API parameters - Daniel Lockyer
* Fixed update notification showing after upgrade - Naz
* Fixed version update indicator on about page - Naz
* Fix test email member uuid (#12809) - Matthew Schmoyer
* Removed netjet support - Hannah Wolfe

[3.177.1]
* Update Ghost to 4.9.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.9.1)
* Upgrade Gscan to 4.1.1 - Sam Lord
* Fixed small text in Gmail on Android for newsletters containing images - Kevin Ansfield

[3.177.2]
* Update Ghost to 4.9.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.9.2)
* Fixed a gscan bug that was causing Ghost to throw http 500s. - Thibaut Patel

[3.177.3]
* Update Ghost to 4.9.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.9.3)
* Fixed "Cannot destructure property" error when overwriting active theme - Kevin Ansfield

[3.177.4]
* Update Ghost to 4.9.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.9.4)
* Fixed name is not defined error when uploading invalid theme

[3.178.0]
* Update Ghost to 4.10.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.10.0)
* Fixed permissible method for Integration Model - Fabien O'Carroll
* Fixed 500 error when creating Products - Fabien O'Carroll
* Fixed member cookies remaining after signout - Fabien O'Carroll
* Fixed gscan path issues on windows - Thibaut Patel
* Fixed error on saving member with existing label - Rishabh Garg
* Fixed alt="null" for feature image in emails - Kevin Ansfield
* Fixed re-authenticate modal not showing when saving in editor - Kevin Ansfield
* Fixed API errors when including member counts for labels - Rishabh Garg

[3.178.1]
* Update Ghost to 4.10.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.10.1)
* Fixed GScan error when handling partials with undefined names - Thibaut Patel

[3.178.2]
* Update Ghost to 4.10.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.10.2)
* Fixed unsubscribed members receiving email when a post is sent to all members (#13181) - Kevin Ansfield

[3.179.0]
* Update Ghost to 4.11.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.11.0)
* Fixed publication icon in nav bar sometimes appearing inverted in dark mode

[3.180.0]
* Update Ghost to 4.12.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.12.0)
* Fixed Admin hitting dead API response on Stripe connect - Rishabh Garg
* Fixed incorrect MRR value on dashboard - Rishabh Garg

[3.180.1]
* Update Ghost to 4.12.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.12.1)

[3.181.0]
* Update Ghost to 4.13.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.13.0)

[3.182.0]
* Update Ghost to 4.14.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.14.0)
* Added new Tiers feature to Beta features section - Rishabh
* Added "Email call to action" card for an email-only CTA targeting free or paid members (#2080) - Kevin Ansfield
* Added ability to update snippet contents (#2073) - Kevin Ansfield
* Merged About into a redesigned What's New page - Peter Zimon
* Added ability to drop an image card onto an empty gallery - Kevin Ansfield
* Added ability to upload a feature image by drag and dropping an image file - Kevin Ansfield
* Added a "saved" indicator for autosaved draft posts - Kevin Ansfield
* Fixed crash when newly created images are dragged into a gallery - Kevin Ansfield
* Fixed captions becoming uneditable when they have a trailing space - Kevin Ansfield

[3.183.0]
* Update Ghost to 4.15.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.15.0)
* Fixed remote command injection when using sendmail email transport - Daniel Lockyer
* Fixed error in sitemap with >50k posts (#13317) - Hannah Wolfe
* Fixed Outlook incorrect text styling and &apos; appearing in email content (#13313) - Kevin Ansfield

[3.183.1]
* Update Ghost to 4.15.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.15.1)
* Fixed member email change vulnerability (see Security Advisory) - Fabien O'Carroll
* Fixed sending emails via SES or non-standard SMTP config - Daniel Lockyer

[3.184.0]
* Update Ghost to 4.16.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.16.0)
* Fixed misleading message when scheduling an already emailed post - Kevin Ansfield

[3.185.0]
* Update Ghost to 4.17.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.17.0)
* Added Members Filtering feature - Fabien O'Carroll
* Fixed incorrect unsaved changes popup on memberships screen - Rishabh Garg
* Fixed mail config not respecting disabled secure connections - Daniel Lockyer

[3.185.1]
* Update Ghost to 4.17.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.17.1)
* Fixed 500 when instance has an invalid redirects config - Naz

[3.186.0]
* Make sendmail optional

[3.187.0]
* Update Ghost to 4.18.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.18.0)
* sparkles Promoted email-only feature to general availability - Naz
* bug Fixed showing "reschedule" in PSM after the post is sent - Naz
* bug Fixed alert notifications being sent out to non-active users - Naz
* bug Fixed copy-to-clipboard buttons in Chrome + Firefox - Kevin Ansfield
* bug Fixed immediately sent email when scheduling email-only post - Naz

[3.188.0]
* Update Ghost to 4.19.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.19.0)
* Added basic {{match}} helper - Hannah Wolfe
* Fixed incorrect publish type showing in publish menu after close/re-open - Kevin Ansfield
* Fixed inability to re-schedule a scheduled post - Kevin Ansfield

[3.188.1]
* Update Ghost to 4.19.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.19.1)
* Fixed handling of "incomplete" subscriptions - Fabien O'Carroll
* Fixed "Send" publish type not being reset when closing publish menu - Kevin Ansfield

[3.189.0]
* Update Ghost to 4.20.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.20.1)
* sparkles Added ability for themes to define custom settings (#13661) - Kevin Ansfield
* Added Offers feature to Ghost - Fabien O'Carroll
* Improved Ghost boot time and memory usage by lazy loading routes - Daniel Lockyer
* Fixed inconsistent publish/send messaging in publish menu - Kevin Ansfield
* Fixed URLs not being correctly transformed during insert operations (#13618) - Kevin Ansfield
* Fixed "Send" publish type not being reset when closing publish menu - Kevin Ansfield

[3.189.1]
* Update Ghost to 4.20.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.20.2)
* Fixed offers and custom theme setting features not appearing in Admin - Kevin Ansfield

[3.189.2]
* Update Ghost to 4.20.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.20.3)
* Fixed error when a theme does not have a "config" object in it's package.json - Kevin Ansfield

[3.189.3]
* Update Ghost to 4.20.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.20.4)
* Fixed 500 error when visiting an email-only post link - Naz
* Fixed GScan crash on files starting with package.json - Thibaut Patel
* Fixed GScan falsely triggering the "unused theme setting" error - Thibaut Patel
* Fixed error from custom settings sync when theme name contains . chars - Kevin Ansfield

[3.190.0]
* Update Ghost to 4.21.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.21.0)
* Fixed settings previews not working in private site mode (TryGhost/Admin#2128) - Rishabh Garg

[3.191.0]
* Update Ghost to 4.22.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.22.0)
* Update Ghost cli to 1.18.0
* Added card-asset config with sensible default - Hannah Wolfe
* Updated Casper to v4.2.0 - Sodbileg Gansukh
* Fixed a 500 error when uploading invalid routes.yaml - Naz
* Fixed broken assets for theme/design preview - Thibaut Patel
* Fixed gscan crashing on invalid package.json - Thibaut Patel
* Prevented unexecuted migrations from being rolled back - Fabien O'Carroll
* Fixed handling of non-subscription invoices - Fabien O'Carroll
* Fixed email type when creating Members via API - Fabien O'Carroll
* Fixed error in setting page access to tiers - Rishabh
* Fixed idempotentcy of addPermissionToRole util (#13685) - Fabien 'egg' O'Carroll

[3.191.1]
* Update Ghost to 4.22.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.22.1)
* Fixed theme installs from ghost.org/marketplace - Kevin Ansfield
* Fixed login problems in Safari on private sites that have front-end/admin on different domains - Kevin Ansfield
* Fixed preview modal showing outdated content if opened before autosave is triggered - Kevin Ansfield
* Fixed offers remaining after subscription change - Fabien O'Carroll
* Fixed member activity feed not showing in dashboard - Kevin Ansfield
* Fixed design settings preview using old settings immediately after activating theme - Kevin Ansfield

[3.191.2]
* Update Ghost to 4.22.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.22.2)
* Fixed install of free themes from the marketplace that aren't in the built-in list - Kevin Ansfield
* Fixed setting Tier prices after changing Stripe accounts - Fabien 'egg' O'Carroll
* Fixed extension mismatching .mp4 as invalid - Naz

[3.191.3]
* Update Ghost to 4.22.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.22.2)
* Fixed EACCES error from card assets on boot - Hannah Wolfe

[3.191.4]
* Update Ghost to 4.22.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.22.4)
* Fixed perms error when building public assets - Hannah Wolfe
* Fixed credentials provider for SES mail transport - Daniel Lockyer
* Fixed Casper not being installable from themes list - Kevin Ansfield
* Fixed Unsplash image selector being available in editor when disabled - Kevin Ansfield

[3.192.0]
* Update Ghost to 4.23.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.23.0)
* Restricted Offer name to 40 characters - Fabien O'Carroll
* Fixed intermittent failures with embedding - Daniel Lockyer

[3.193.0]
* Update Ghost to 4.24.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.24.0)
* Added support for theme card_asset config to GScan - Thibaut Patel
* Fixed redirects.json file corruption on upload - Naz
* Fixed /unsplash search term not including all words - Kevin Ansfield
* Fixed card asset init/reload behaviour - Hannah Wolfe

[3.194.0]
* Update Ghost to 4.25.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.25.0)
* Added GIF card to editor - Kevin Ansfield
* Added Button card to editor - Kevin Ansfield
* Added NFT card to editor - Fabien O'Carroll

[3.194.1]
* Update Ghost to 4.25.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.25.1)
* Fixed malformed URLs crashing the url helper - Thibaut Patel
* Fixed 500 errors for Stripe webhooks - Fabien egg O'Carroll
* Fixed Member imports overriding missing columns - Fabien egg O'Carroll

[3.195.0]
* Update Ghost to 4.26.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.26.0)
* Added toggle card to editor - Rishabh Garg
* Added callout card to editor - Thibaut Patel
* Updated Casper to v4.3.1 - Peter Zimon
* Fixed error when sending messages via bootstrap socket - Daniel Lockyer

[3.195.1]
* Update Ghost to 4.26.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.26.1)
* Made toggle card closed by default - Djordje Vlaisavljevic

[3.196.0]
* Update Ghost to 4.27.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.27.1)
* Added an additional blockquote style - Kevin Ansfield
* Added support for rich Twitter embeds in emails - Fabien egg O'Carroll
* Updated Casper to v4.4.0 - Daniel Lockyer
* Added confirmation dialog any time a post/page will be published - Kevin Ansfield
* Fixed pasting html into a KoenigBasicHtmlInput - Thibaut Patel
* Fixed pasted image files appearing twice in editor - Kevin Ansfield
* Fixed initial paste of Twitter URL not triggering full script load - Kevin Ansfield
* Fixed card scripts executing before DOM is parsed - Fabien egg O'Carroll

[3.196.1]
* Update Ghost to 4.27.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.27.2)

[3.197.0]
* Update Ghost to 4.28.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.28.0)
* Added product card to editor - Thibaut Patel
* Updated Casper to v4.5.0 - Daniel Lockyer
* Fixed using @partial-block in templates - Ben Shaw

[3.198.0]
* Update Ghost to 4.29.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.29.0)
* Added audio card to editor - Rishabh Garg

[3.199.0]
* Update Ghost to 4.30.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.30.0)
* Added new video card - Rishabh Garg
* Logged in members after Stripe Checkout - Fabien egg O'Carroll

[3.200.0]
* Update Ghost to 4.31.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.31.0)
* Added file card to editor - Rishabh Garg
* Updated Casper to v4.6.1 - Daniel Lockyer
* Fixed audio file duration missing digit in editor - Sanne de Vries

[3.201.0]
* Update Ghost to 4.32.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.32.0)
* Added header card to editor - Daniel Lockyer
* Updated Casper to v4.7.2 - Daniel Lockyer
* Fixed 500 errors when signing up with invalid email (#13901) - Fabien 'egg' O'Carroll

[3.201.1]
* Update Ghost to 4.32.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.32.1)
* Improved copy on member email unsubscribe page - Kevin Ansfield
* Fixed AMP style compliance for new editor cards - Juan Delgadillo
* Fixed broken Zapier links in Admin integration page - Rishabh Garg
* Fixed incorrect automatic CSV download when bulk-deleting members - Kevin Ansfield

[3.201.1]
* Update Ghost to 4.32.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.32.2)
* Fixed imports ignoring subscribed_to_emails flag - Fabien "egg" O'Carroll
* Fixed broken styles in Admin's search modal - Kevin Ansfield

[3.201.2]
* Update Ghost to 4.32.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.32.3)
* Fixed "Create Post" action error in Zapier when assigning new tags (#13972) - Kevin Ansfield
* Support inline partial execution in children partials - Thaibaut Patel
* Restored support for inline partials and dynamic partials - Thibaut Patel
* Fixed incorrect edit redirect for authors - Matt Hanley
* Fixed missing spacing after blockquote sections in emails viewed on iPads with iOS Mail (#13937) - Kevin Ansfield
* Fixed imports ignoring subscribed_to_emails flag - Fabien "egg" O'Carroll

[3.201.3]
* Update Ghost to 4.33.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.33.0)
* Disabled AMP by default - Fabien 'egg' O'Carroll & Thibaut Patel
* Fixed Offer Redemptions being over counted (#13988) - Fabien 'egg' O'Carroll
* Fixed modal to invite staff users not scrolling in Safari & Chrome - Sanne de Vries

[3.201.4]
* Update Ghost to 4.33.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.33.1)
* Fixed webhook handling of Stripe Checkout - Fabien "egg" O'Carroll
* Fixed crashing on boot with revoked Stripe keys - Fabien "egg" O'Carroll

[3.201.5]
* Update Ghost 4.33.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.33.2)

[3.202.0]
* Update Ghost to 4.34.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.34.0)
* Improved Email newsletter settings page - Sanne de Vries, Peter Zimon and Kevin Ansfield

[3.202.1]
* Update Ghost to 4.34.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.34.1)
* Fixed editor cards not being usable by contributors - Kevin Ansfield
* Fixed the ability to assign Complimentary subscriptions - Fabien "egg" O'Carroll

[3.202.2]
* Update Ghost to 4.34.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.34.2)
* Fixed icon size in member activity view when labs flag is disabled - James Morris

[3.202.3]
* Update Ghost to 4.34.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.34.3)

[3.203.0]
* Update Ghost to 4.35.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.35.0)
* Made /-menu card searching case-insensitive - Kevin Ansfield
* Fixed the event ordering in the member activity feed (#14093) - Thibaut Patel
* Added missing payment events in the member activity feed - Thibaut Patel

[3.204.0]
* Update Ghost to 4.36.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.36.0)
* Fixed labels not being selectable in members importer - Kevin Ansfield
* Fixed post content gating error for themes using old API - Rishabh
* Fixed welcome pages for paid signups - Fabien "egg" O'Carroll
* Fixed welcome pages when Tiers is enabled - Fabien "egg" O'Carroll
* Fixed 500 errors when cancelling subscriptions - Fabien "egg" O'Carroll

[3.204.1]
* Update Ghost to 4.36.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.36.1)
* Fixed "Invalid mobiledoc structure" errors that could occur when saving posts with header cards - Kevin Ansfield

[3.204.2]
* Update Ghost to 4.36.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.36.2)
* Fixed Stripe API disconnection - Naz
* Fixed error handling webhooks for unknown member (#14155) - Fabien 'egg' O'Carroll
* Fixed Admin crash when member filters were focused+blurred without entering a filter value - Kevin Ansfield
* Fixed "Header two" formatting toolbar button not working - Kevin Ansfield
* Fixed Stripe checkout session urls being invalid - Fabien "egg" O'Carroll
* Pinned frontend API version to canary - Hannah Wolfe

[3.204.3]
* Update Ghost to 4.36.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.36.3)
* Fixed saving of custom integrations - Kevin Ansfield
* Fixed "Enable email open-rate" toggle - Kevin Ansfield

[3.205.0]
* Update Ghost to 4.37.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.37.0)
* Fixed mismatched operator in member filters after changing filter type - Kevin Ansfield
* Fixed saving of custom integrations - Kevin Ansfield
* Fixed "Enable email open-rate" toggle - Kevin Ansfield
* Fixed welcome pages not working for "subscribe" links (#14176) - Fabien 'egg' O'Carroll

[3.206.0]
* Update Ghost to 4.38.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.38.0)
* Added "Created", "Paid start date", and "Next billing date" date-based filters to members list screen (TryGhost/Admin#2290) - Kevin Ansfield
* Added >, <, >=, and <= operators to match helper (#14215) - Simon Backx
* Updated Casper to v4.7.3 - Daniel Lockyer
* Updated Member page layout (TryGhost/Admin#2271) - Sanne de Vries
* Removed empty benefits before saving (TryGhost/Admin#2284) - Simon Backx
* Fixed sending non-integer prices to tiers api (TryGhost/Admin#2288) - Rishabh Garg
* Fixed members table showing dates in UTC rather than site timezone - Kevin Ansfield
* Fixed incorrect {{access}} property when using get/next-post/prev-post helpers (#14256) - Simon Backx
* Fixed missing index page from pages sitemaps - Naz

[3.206.1]
* Update Ghost to 4.38.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.38.1)
* Fixed theme activation with capitalized names - Naz

[3.207.0]
* Update Ghost to 4.39.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.39.0)
* Added support for multiple tiers (#14311) - Rishabh Garg
* Added "Name" and "Email" filters to members screen - Kevin Ansfield
* Added "contains" operator support to ?filter= query params (#14286) - Kevin Ansfield
* Added "Last seen" filter to members screen - Kevin Ansfield

[3.207.1]
* Update Ghost to 4.39.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.39.1)
* Fixed slow loading and high memory usage of members list screen - Kevin Ansfield

[3.208.0]
* Update Ghost to 4.40.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.40.0)
* Fixed member "last seen at" data not being returned in the API - Kevin Ansfield
* Fixed post access in the get helper (#14282) - Fabien 'egg' O'Carroll
* Fixed duplicate email open rate column when filtering members by open rate - Kevin Ansfield
* Fixed slow loading and high memory usage of members list screen - Kevin Ansfield
* Fixed scheduled date/time inputs not being focusable when used from post preview modal - Kevin Ansfield

[3.209.0]
* Update Ghost to 4.41.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.41.0)
* Added members activity feed screen to admin area - Kevin Ansfield
* Added canceled subscriptions in member detail screen (TryGhost/Admin#2287) - Thibaut Patel
* Fixed /canary/ API endpoints 404s - Naz

[3.209.1]
* Update Ghost to 4.41.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.41.2)
* Fixed error on saving a paid member (#2312) - Rishabh Garg
* Fixed offer redemption count for members not updating - Thibaut Patel
* Fixed offer discount information not shown in member's account - Thibaut Patel

[3.209.2]
* Update Ghost to 4.41.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.41.3)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[3.210.0]
* Update Ghost to 4.42.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.42.0)
* Fixed non-functioning "Leave" button on unsaved-changes confirmation modals - Kevin Ansfield
* Fixed scheduled send-only post switching to publish+send when rescheduling - Kevin Ansfield
* Fixed __GHOST_URL__ tranformations in video cards - Naz
* Fixed broken links in some of theme errors - Naz

[3.210.1]
* Update Ghost to 4.42.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.42.1)
* Fixed incorrect modal heights - Kevin Ansfield
* Fixed incorrect modal and theme preview positioning - Kevin Ansfield

[3.211.0]
* Update Ghost to 4.43.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.43.0)

[3.211.1]
* Update Ghost to 4.43.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.43.1)
* Fixed false positive fatal error for {{tiers}} - Naz

[3.212.0]
* Update Ghost to 4.44.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.44.0)
* Fixed close button on "unsaved changes" modal not always behaving like "Stay" button - Kevin Ansfield

[3.213.0]
* Update Ghost to 4.45.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.45.0)
* Fixed common build issues with SQLite - Daniel Lockyer
* Dropped support for Node 12 - Daniel Lockyer

[3.214.0]
* Update Ghost to 4.46.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.46.0)
* Added ability to set sender name on newsletters
* Added ability to re-name newsletter
* Added support for toggling auto-opt in preferences for newsletters

[3.214.1]
* Update Ghost to 4.46.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.46.1)
* Fixed null values in settings default newsletter migration (#14639) - Simon Backx

[3.214.2]
* Update Ghost to 4.46.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.46.2)
* Fixed error when firing Members webhooks (#14645) - Fabien 'egg' O'Carroll

[3.215.0]
* Update Ghost to 4.47.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.47.0)
* Fixed newsletters' header_image saved as absolute url (#14690) - Simon Backx
* Fixed error when trying to update a non-existent member - Simon Backx

[3.215.1]
* Update Ghost to 4.47.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.47.1)
* Fixed wrong newsletter used when sending scheduled post (#14732) - Simon Backx
* Fixed members CSV export not filtering on subscribed (#14724) - Simon Backx
* Fixed importing posts with a newsletter assigned - Matt Hanley

[3.215.2]
* Update Ghost to 4.47.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.47.2)
* Fixed email stats not working for EU Mailgun endpoints - Matt Hanley

[3.215.3]
* Update Ghost to 4.47.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.47.3)
* Fixed sending newsletters with header images (#14828) - Simon Backx

[3.215.4]
* Update Ghost to 4.47.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.47.4)
* Fixed fetching newsletter relation when sending newsletters (#14831) - Simon Backx

[3.216.0]
* Update Ghost to 4.48.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v4.48.0)
* Added Stripe data, benefits, offers, products and snippets to exporter - Hannah Wolfe & Matt Hanley
* Fixed member exports timing out for large sites (#14876) - Simon Backx

[4.0.0]
* Update Ghost to 5.0.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.0.0)
* [Breaking changes](https://ghost.org/docs/changes/#ghost-50)
* Added support for Multiple Newsletters - Rishabh Garg
* Added new analytics dashboard - Fabien 'egg' O'Carroll
* Added new publishing workflow - Kevin Ansfield
* Added theme helpers for detecting member features - Hannah Wolfe
* Fixed member exports timing out for large sites (#14876) (#14878) - Simon Backx
* Fixed importing posts with a newsletter assigned - Matt Hanley
* Fixed newsletters' header_image saved as absolute url (#14690) - Simon Backx
* Removed versioned APIs - Hannah Wolfe
* Removed legacy product + price helpers from themes - Hannah Wolfe

[4.0.1]
* Update Ghost to 5.0.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.0.1)
* Fixed unreliable paid members enabled checks - Simon Backx
* Fixed email analytics crashing when processing unsubscribe/complaint events - Kevin Ansfield
* Fixed signing key identification in JWKs - Naz
* Fixed inability to select a date in future months when scheduling - Kevin Ansfield
* Fixed page vs post preview template picking - Naz
* Fixed page vs post context calculation - Naz
* Fixed signing key mismatching in members JWT/JWKS - Naz
* Fixed timezone related issues when scheduling posts - Kevin Ansfield

[4.0.2]
* Update Ghost to 5.0.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.0.2)
* Fixed editor cards sometimes being removed as soon as they are added

[4.1.0]
* Update Ghost to 5.1.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.1.0)
* Allow page to be used as post in dynamic routing - Hannah Wolfe
* Updated Casper to v5.1.1 - Daniel Lockyer
* Reduced favicon requirements and added image formatting (#14918) - Simon Backx
* Fixed reading time for RTL languages - Hannah Wolfe
* Fixed paste into product card descriptions not stripping header formatting - Kevin Ansfield

[4.1.1]
* Update Ghost to 5.1.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.1.1)
* Fixed broken file selectors in labs, general, and portal settings screens - Kevin Ansfield

[4.2.0]
* Update Ghost to 5.2.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.2.0)
* Updated Casper to v5.1.3 - Daniel Lockyer
* Improved save/update button copy in editor (#2413) - Kevin Ansfield
* Fixed Stripe Checkout for Members w/ existing subscriptions (#14953) - Fabien 'egg' O'Carroll
* Fixed invalid user role assignment - Naz
* Fixed default publish type being "Publish and email" when default recipients set to "Usually nobody" - Kevin Ansfield

[4.2.1]
* Update Ghost to 5.2.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.2.1)
* Fixed an error when updating a user - Naz

[4.2.2]
* Update Ghost to 5.2.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.2.2)
* Updated Casper to v5.1.4 - Sodbileg Gansukh
* Fixed duplicate tiers being created on import (#14964) - Matt Hanley

[4.2.3]
* Update Ghost to 5.2.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.2.3)
* Fixed RCE exploit with date helper & locale setting (see advisory) - Fabien "egg" O'Carroll

[4.2.4]
* Update Ghost to 5.3.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.2.4)
* Fixed schedule post date picker sometimes not allowing "today" to be selected - Kevin Ansfield
* Fixed scheduled post datepicker sometimes picking day before selected date - Kevin Ansfield

[4.3.0]
* Update Ghost to 5.3.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.3.0)
* Added built-in site search functionality - Rishabh Garg
* Added Ghost Explore endpoint - Daniel Lockyer
* Updated Casper to v5.2.0 - Sodbileg Gansukh
* Fixed "Published" date for recent posts in dashboard always showing current time - Kevin Ansfield
* Fixed issue with excerpt field in API - Ronald Langeveld
* Fixed dashboard member activity feed always showing current time for timestamp - Kevin Ansfield

[4.3.1]
* Update Ghost to 5.3.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.3.1)
* Fixed cookies when running Ghost without SSL - Fabien 'egg' O'Carroll

[4.4.0]
* Update Ghost to 5.4.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.4.0)
* Redirected Members to previous post/page upon sign-in - Fabien 'egg' O'Carroll
* Added member count helpers (#15013) - Ronald Langeveld
* Updated Casper to v5.2.1 - Sodbileg Gansukh

[4.4.1]
* Update Ghost to 5.4.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.4.1)
* Fixed new member count helpers not included in GScan - Daniel Lockyer
* Fixed sending multiple support email verification emails (#15044) - Simon Backx

[4.5.0]
* Update Ghost to 5.5.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.5.0)
* Updated default config and CDN for frontend apps - Rishabh Garg
* Updated Casper to v5.2.2 - Sodbileg Gansukh

[4.6.0]
* Update Ghost to 5.6.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.6.0)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[4.7.0]
* Update Ghost to 5.7.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.7.0)
* Added format option to img-url helper (#14962) - Simon Backx
* Extended paywall card for newsletters - Rishabh Garg

[4.7.1]
* Update Ghost to 5.7.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.7.1)
* Updated Casper to v5.2.3 - Sodbileg Gansukh
* Fixed pasting into the post tags input not working - Kukhyeon Heo
* Fixed impersonate modal not closing correctly when navigating away from member page - Scott Beinlich
* Fixed markdown card lacking superscripts & subscripts - Scott Beinlich

[4.8.0]
* Update Ghost to 5.8.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.8.0)
* Added Ghost Explore integration - Aileen Nowak
* Upgraded Tenor API to v2
* Fixed missing published Admin assets when running in development - Daniel Lockyer

[4.8.1]
* Release database migration lock on startup to avoid potential race on app update

[4.8.2]
* Update Ghost to 5.8.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.8.1)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[4.8.3]
* Update Ghost to 5.8.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.8.2)
* Fixed saving membership settings (#15196) - Fabien 'egg' O'Carroll

[4.8.4]
* Update Ghost to 5.8.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.8.3)
* Fixed Admin UI freezing when interacting with dropdown lists - Kevin Ansfield

[4.9.0]
* Update Ghost to 5.9.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.9.1)
* Added native comments (#15223) - Simon Backx
* Updated Casper to v5.3.0 - Sodbileg Gansukh
* Fixed packaging issue with missing component - Daniel Lockyer

[4.9.1]
* Update Ghost to 5.9.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.9.2)
* Fixed unsubscribe flow for comment reply emails (#15232) - Simon Backx

[4.9.2]
* Update Ghost to 5.9.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.9.3)
* Fixed comments script always injected - Simon Backx

[4.9.3]
* Update Ghost to 5.9.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.9.4)

[4.10.0]
* Update Ghost to 5.10.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.10.0)
* Fixed error deleting post with comment replies - Hannah Wolfe
* Fixed broken email prefs link in comment emails - Hannah Wolfe
* Fixed comments not visible in activity feed - Simon Backx
* Fixed line breaks not persisting when used inside email cards - Kevin Ansfield
* Fixed adding emojis in editor - Daniel Lockyer

[4.10.1]
* Update Ghost to 5.10.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.10.1)
* Fixed packaging issue with cached dependencies - Daniel Lockyer

[4.11.0]
* Update Ghost to 5.11.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.11.0)
* Allowed setting expiry for complimentary subscriptions - Rishabh Garg
* Enabled free trials via tiers and offers - Rishabh Garg
* Fixed empty error csv file for member imports (#15274) - Rishabh Garg
* Added secret handling for webhooks (#13980) - Regrau

[4.12.0]
* Update Ghost to 5.12.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.12.0)
* Enabled member email alerts - Rishabh Garg
* Added {{search}} theme helper - Hannah Wolfe

[4.12.1]
* Update Ghost to 5.12.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.12.1)
* Fixed newsletters not rendering with non-HTML safe chars (#15331) - Fabien 'egg' O'Carroll

[4.12.2]
* Update Ghost to 5.12.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.12.2)
* Fixed commenting on tier-only posts (#15333) - Simon Backx
* Fixed removing comped subscriptions for members with active subs (#15332) - Simon Backx

[4.12.3]
* Update Ghost to 5.12.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.12.3)
* Fixed rate limiting for user login (#15336) - Fabien 'egg' O'Carroll
* Fixed email alerts for paid members on import (#15347) - Rishabh Garg

[4.12.4]
* Update Ghost to 5.12.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.12.4)
* Fixed paid email preview stopped working in emails (#15356) - Simon Backx

[4.13.0]
* Update Ghost to 5.13.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.13.0)
* Added logging configuration option for timestamps to use the local timezone - Daniel Lockyer
* Fixed sending emails from email domain that includes www subdomain (#15348) - Simon Backx
* Fixed image width/height and links not being preserved when pasting or importing html (#15350) - Kevin Ansfield

[4.13.1]
* Update Ghost to 5.13.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.13.1)
* Fixed paid subscription alert showing incorrect offer amount - Rishabh

[4.13.2]
* Update Ghost to 5.13.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.13.2)
* Fixed OpenSea NFT OEmbeds (#15372) - Fabien 'egg' O'Carroll

[4.14.0]
* Update Ghost to 5.14.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.14.0)
* Added history log for staff actions - Daniel Lockyer
* Added optional data-attribute to enable and disable auto redirection. (#15335) - Ronald Langeveld
* Fix sidebar members number not updated after member create & delete (#15378) - Hakim Razalan
* Fixed validation errors for duplicate members (#15362) - Hakim Razalan
* Fixed square brackets being % encoded in URLs (#14977) - rw4nn
* Fixed error when deleting tag and missing slugs on tags list - Kevin Ansfield
* Fixed product card images causing very wide emails in Outlook (#15374) - Kevin Ansfield
* Fixed incorrect member count on sidebar (#15330) - Ronald Langeveld
* Fixed archived tiers appearing in Portal Links UI (#15351) - Hakim Razalan

[4.14.1]
* Update Ghost to 5.14.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.14.1)
* Fixed Admin freeze when filtering long tag lists - Kevin Ansfield

[4.14.2]
* Update Ghost to 5.14.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.14.2)
* Fixed feature image caption escaped twice in newsletters (#15417) - Simon Backx

[4.15.0]
* Update Ghost to 5.15.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.15.0)
* Added support for .m4a format in audio cards - Naz
* Added number formatting to members import email - Simon Backx
* Fixed tag slug not auto-matching tag title when creating new tag - Kevin Ansfield
* Fixed duplicate error columns in members import error CSV - Simon Backx
* Fixed Hidden error in "Add tier" modal (#15361) - Hakim Razalan

[4.16.0]
* Update Ghost to 5.16.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.16.0)
* Fixed distorted images in newsletters for product cards - Kevin Ansfield
* Switched to jsDelivr CDN endpoint with shorter browser cache - Daniel Lockyer

[4.16.1]
* Update Ghost to 5.16.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.16.1)
* Fixed confusing error state when publishing if member count is over hosting plan limit - Kevin Ansfield
* Fixed default content CTA message to reflect page vs post - Rishabh Garg
* Fixed product card not displaying with just an image+button - Kevin Ansfield
* Fixed deleting users with draft posts - Simon Backx

[4.16.2]
* Update Ghost to 5.16.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.16.2)
* Fixed error preventing publish for non-Admin staff users - Kevin Ansfield

[4.17.0]
* Update Ghost to 5.17.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.17.0)
* Added email click tracking - Simon Backx

[4.17.1]
* Update Ghost to 5.17.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.17.1)
* Fixed broken activity feed and click filter - Simon Backx

[4.17.2]
* Update Ghost to 5.17.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.17.2)
* Fixed magic link endpoint sending multiple emails - Fabien "egg" O'Carroll
* Prevented member creation when logging in (#15526) - Simon Backx

[4.18.0]
* Update Ghost to 5.18.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.18.0)
* Updated Casper to v5.3.2 - Sodbileg Gansukh
* Improved preview text on member alert emails (#15543) - Elena Baidakova
* Improved email failure handling and retrying (#15504) - Simon Backx
* Fixed member importer crash for failed imports (#15560) - Rishabh Garg
* Fixed timezone issue with min/max dates in datetime picker - Ozan Uslan
* Fixed ctrl/cmd+s not saving focused fields on general/staff settings screens - Kevin Ansfield
* Fixed broken close buttons on modals (#15514) - Ronald Langeveld
* Fixed active state bug in sidebar nav (#15511) - Stephen Sauceda

[4.19.0]
* Update Ghost to 5.19.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.19.0)
* Removed redirects from search engine indexing (#15617) - Fabien 'egg' O'Carroll
* Fixed embedded cards for non-utf8 content (#15578) - jbenezech
* Fixed redirects with special characters (#15533) - Prathamesh Gawas
* Fixed missing accent color for default content cta (#15611) - Rishabh Garg
* Fixed color of member alert email links (#15582) - Elena Baidakova
* fixed error message code for HB translate helper (#15529) - Christa
* Fixed sitemaps with no content (#15571) - jbenezech
* Fixed settings overriden when updated from multiple tabs (#15536) - jbenezech
* Fixed note field keyboard save in admin members form (#15476) - AmbroziuBaban

[4.19.1]
* Update Ghost to 5.19.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.19.1)
* Fixed 404 collection links for new tags - Naz

[4.19.2]
* Update Ghost to 5.19.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.19.3)

[4.20.0]
* Update Ghost to 5.20.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.20.0)
* Allowed fixing newsletter links (#15672) - Rishabh Garg

[4.21.0]
* Update Ghost to 5.21.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.21.0)
* Added audience feedback - Simon Backx
* Added member growth sources - Simon Backx
* Added Ghost Explore app - Aileen Nowak
* Fixed comped member creation via Admin API (#15714) - Rishabh Garg
* Fixed missing unsaved changes modal for member newsletters (#15564) - Hakim Razalan
* Fixed redirect to signin modal not shown when logged out (#15522) - Hakim Razalan

[4.22.0]
* Update Ghost to 5.22.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.0)
* Fixed Tiers importer not correctly mapping price data

[4.22.1]
* Updaet Ghost to 5.22.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.1)
* Fixed error preventing admin area being usable by staff users with Contributor role - Kevin Ansfield
* Fixed missing active theme breaks design screen (#15602) - Arjuna Kristophe Sankar

[4.22.2]
* Update Ghost to 5.22.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.2)
* Fixed Tier description not being set (#15741) - Fabien 'egg' O'Carroll

[4.22.3]
* Update Ghost to 5.22.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.4)
* Fixed errors with Stripe Checkout (#15749) - Fabien 'egg' O'Carroll

[4.22.4]
* Update Ghost to 5.22.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.5)
* Fixed importer importing invalid Tier pricing data - Fabien "egg" O'Carroll
* Fixed archiving Tiers (#15761) - Fabien 'egg' O'Carroll

[4.22.5]
* Update Ghost to 5.22.6
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.6)
* Add comma separation to all numbers in comments - e.baidakova
* Fixed comped subscription duration drop-down sometimes not being visible (#15764) - Kevin Ansfield
* Fixed archiving Tiers (#15761) - Fabien 'egg' O'Carroll
* Add ability to cache comments count endpoint - e.baidakova

[4.22.6]
* Update Ghost to 5.22.7
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.7)
* Fixed upgrading Subscriptions to new Tiers - Fabien "egg" O'Carroll

[4.22.7]
* Update Ghost to 5.22.8
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.8)
* Fixed source tracking using cached value (#15778) - Simon Backx
* Fixed sending feedback on email only posts - Simon Backx

[4.22.8]
* Update Ghost to 5.22.9
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.9)
* Handled deleted Stripe objects in the Stripe Checkout flow - Fabien "egg" O'Carroll

[4.22.9]
* Update Ghost to 5.22.10
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.10)
* Updated Casper to v5.4.0 - Ghost CI
* Fixed question marks replacement for some characters in Outlook (#15801) - Simon Backx
* Fixed offer links with an archived tier (#15792) - Simon Backx
* Fixed visible canceled events in conversions tab on analytics page (#15796) - Simon Backx
* Fixed pasting newlines in post titles (#15794) - Simon Backx
* Fixed link click counts for duplicate links (#15789) - Simon Backx
* Fixed ref attribute in email links (#15775) - Simon Backx
* Fixed complimentary_plan Member imports - Naz
* Fixed icons size for Outlook (#15772) - Elena Baidakova

[4.22.10]
* Update Ghost to 5.22.11
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.22.11)
* Fixed error when importing members - Simon Backx

[4.23.0]
* Update Ghost to 5.23.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.23.0)
* Added specific newsletter support for bulk unsubscribes (#15742) - Ronald Langeveld
* Updated Casper to v5.4.1 - Ghost CI
* Fixed race condition when sending email (#15829) - Simon Backx
* Fixed amp-youtube being too small (#15826) - Jacob Simon

[4.24.0]
* Update Ghost to 5.24.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.24.0)

[4.24.1]
* Update Ghost to 5.24.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.24.1)
* Disabled editable relations by default - Naz

[4.24.2]
* Update Ghost to 5.24.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.24.2)
* Fixed verification trigger not working for large imports

[4.25.0]
* Update Ghost to 5.25.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.25.0)
* Disabled editable relations by default - Naz

[4.25.1]
* Update Ghost to 5.25.1
* Update Ghost cli to 1.23.1
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.25.1)
* Fixed setting delivered_at to null after hard bounce (#15942) - Simon Backx
* Fixed errors of old events from deleted members (#15944) - Simon Backx

[4.25.2]
* Update Ghost to 5.25.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.25.2)
* Fixed broken redemption count for offers (#15954) - Rishabh Garg

[4.25.3]
* Update Ghost to 5.25.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.25.3)
* Fixed free trial applied alongside an offer in checkout (#15975) - Rishabh Garg
* Fixed email header images serving original image size (#15950) - Simon Backx
* Hid the analtyics page for editors - Simon Backx
* Fixed unexpected "unsaved changes" modal when deleting a member - Kevin Ansfield

[4.25.4]
* Update Ghost to 5.25.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.25.4)
* Reverted Sentry to v7.11.1 to fix unhandled promise rejection crashes - @SimonBackx

[4.25.5]
* Update Ghost to 5.25.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.25.5)
* Updated Casper to v5.4.2 - Ghost CI
* Fixed slug saving in editor (#16007) - Elena Baidakova
* Handled unknown Mailgun events (#15995) - Simon Backx
* Removed expired offers shown in portal account detail - Rishabh
* Fixed 'Invalid status code: undefined' in members api (#15973) - Simon Backx
* Removed horizontal scroll for long author's name (#15985) - Elena Baidakova

[4.26.0]
* Update Ghost to 5.26.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.26.0)
* Added Revue Importer (#16012) - Hannah Wolfe
* Added html -> mobiledoc conversion to the importer (#16016) - Hannah Wolfe
* Added theme docs link to the design settings (#16014) - Sodbileg Gansukh
* Added newsletter subscription filtering to members (#16006) - Ronald Langeveld
* Updated Casper to v5.4.4 - Ghost CI
* Fixed invalid email getting saved for members (#16021) - Rishabh Garg
* Fixed archived offers return button not working (#16023) - Simon Backx
* Fixed importing existing member resetting newsletters (#16017) - Simon Backx

[4.26.1]
* Update Ghost to 5.26.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.26.1)

[4.26.2]
* Update Ghost to 5.26.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.26.2)
* Fixed meta is missing error with revue imports (#16033) - Hannah Wolfe

[4.26.3]
* Update Ghost to 5.26.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.26.3)

[4.26.4]
* Update Ghost to 5.26.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.26.4)

[4.27.0]
* Update Ghost to 5.27.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.27.0)
* bug Fixed admin loading member counts for authors and editors - Simon Backx
* bug Fixed batches can have an empty "to" field (#16064) - Simon Backx
* bug Removed free trial message shown on portal for invite only sites - Rishabh
* bug Fixed SingleUseTokens being cleared on boot (#15999) - Simon Backx
* bug Fixed missing validation of offer amounts in the admin panel (#16022) - Kevin Ansfield
* bug Fixed tiers order by monthly_price (#16013) - Elena Baidakova

[4.28.0]
* Update Ghost to 5.28.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.28.0)
* Added visible theme errors in admin - Simon Backx
* Fixed duplicate member columns when filtering - Simon Backx
* Fixed warnings not visible when uploading theme with fatal errors - Simon Backx
* Stopped creating redundant Stripe Customers for Members - Patrick McKenzie

[4.29.0]
* Update Ghost to 5.29.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.29.0)

[4.30.0]
* Update Ghost to 5.30.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.30.0)
* sparkles Improved newsletter delivery rates and email suppression handling - Fabien 'egg' O'Carroll
* sparkles Added redeemed offers filtering for members (#16071) - Ronald Langeveld
* art Add ability to send test email with chosen newsletter (#15783) - Elena Baidakova
* bug Fixed ECONNRESET error when connecting to Azure MySQL DB - Daniel Lockyer
* bug Fixed storing email recipient failures - Simon Backx
* bug Fixed feedback buttons for dark mode (#16091) - Elena Baidakova
* fire Removed support for {{lang}} helper - Naz

[4.30.1]
* Update Ghost to 5.30.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.30.1)

[4.31.0]
* Update Ghost to 5.31.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.31.0)
* Improved email stability (#16159) - Rishabh Garg
* Updated Casper to v5.4.5 - Ghost CI
* Fixed post links being marked as edited when they were not (#16153) - Simon Backx
* Fixed suppression list data for emails with plus sign (#16140) - Simon Backx

[4.32.0]
* Update Ghost to 5.32.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.32.0)
* Fixed email segment generation (#16182) - Simon Backx
* Fixed invalid expiry for member tier subscriptions (#16174) - Rishabh Garg
* Reduced concurrency when fetching Mailgun events (#16176) - Simon Backx

[4.33.0]
* Update Ghost to 5.33.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.0)
* Added referrals invite notification (#16187) - Aileen Booker
* Fixed not reactivating email analytics jobs - Simon Backx

[4.33.1]
* Update Ghost to 5.33.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.1)
* Fixed HTML escaping of feature_image_caption in newsletters - Simon Backx
* Fixed newsletter resending showing incorrect recipient data - Fabien 'egg' O'Carroll

[4.33.2]
* Update Ghost to 5.33.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.2)

[4.33.3]
* Update Ghost to 5.33.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.3)
* Fixed blank email previews when opening from member activity feeds (#16207) - Kevin Ansfield
* Fixed email header image width not set (#16210) - Simon Backx

[4.33.4]
* Update Ghost to 5.33.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.4)

[4.33.5]
* Update Ghost to 5.33.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.5)

[4.33.6]
* Update Ghost to 5.33.6
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.6)
* Fixed link to posts on audio cards in emails - Simon Backx

[4.33.7]
* Update Ghost to 5.33.7
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.7)

[4.33.8]
* Update Ghost to 5.33.8
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.33.8)
* Fixed default email recipients always being "everyone" rather than matching post visibility (#16247) - Kevin Ansfield
* Fixed throwing errors during link redirects - Simon Backx

[4.34.0]
* Update Ghost to 5.34.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.34.0)
* Fixed email replacements without fallback - Simon Backx
* Fixed storing email failures with an empty message (#16260) - Simon Backx
* Fixed subscriptions visible as "Active" within Ghost Admin (#16255) - Simon Backx
* Fixed emoji causing page jump in safari (#16026) - Tihomir Valkanov
* Fixed members tier filtering (#16212) - Ronald Langeveld
* Fixed storing original files for images (#16117) - Simon Backx

[4.34.1]
* Update Ghost to 5.34.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.34.1)

[4.35.0]
* Update Ghost to 5.35.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.35.0)
* art Improved meta description preview - John O'Nolan
* bug Fixed uploading images with custom storage adapters - Simon Backx

[4.35.1]
* Update Ghost to 5.35.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.35.1)

[4.36.0]
* Update Ghost to 5.36.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.36.0)
* Added outbound link tagging setting (#16324) - Simon Backx

[4.36.1]
* Update Ghost to 5.36.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.36.1)
* Fixed the fetching of additional tweet data from Twitter API when embedding tweets

[4.37.0]
* Update Ghost to 5.37.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.37.0)
* Added document file support to the zip importer - Naz
* Added media file support to the zip importer - Naz
* Updated Casper to v5.4.7 - Ghost CI
* Fixed broken link filter params (#16351) - Ronald Langeveld
* Added undefined error handling to failed uploads (#15982) - Ronald Langeveld
* Fixed uploads of m4a files with audio/mp4 content type - Reupen Shah
* Fixed failing update-check job - Naz
* Fixed Handlebars’ asset helper for SafeString input - monkey sees

[4.38.0]
* Update Ghost to 5.38.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.38.0)
* xcludes sites with `canonical_url` meta from sitemap. (#16376) - Ronald Langeveld
* Added source attribution info to email alerts (#16360) - Rishabh Garg
* Added newsletter preference page link in portal settings - Rishabh
* Fixed free trials not visible in paid subscriptions graph - Simon Backx
* Fixed 3D secure payment not counted as paid subscription in graph - Simon Backx
* Fixed retrying failed emails when rescheduling them (#16383) - Simon Backx
* Fixed HTML escaping when using outbound link tagging (#16380) - Simon Backx
* Fixed replacements with fallback in plaintext newsletters (#16372) - Simon Backx
* Fixed ability to cancel Stripe subscription when deleting member - Simon Backx

[4.39.0]
* Update Ghost to 5.39.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.39.0)
* sparkles Added comment CTA in newsletters - @SimonBackx
* sparkles Added option to remove post title from newsletters - @SimonBackx
* bug Fixed upper price limit in Membership Tiers - @sagzy
* bug Fixed editor crashing with unknown cards in mobiledoc - @cmraible
* bug Fixed mainEntityOfPage property within page metadata - @daniellockyer

[4.40.0]
* Update Ghost to 5.40.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.40.0)
* Added subscription details to newsletters - Simon Backx
* Added latests posts to newsletters - Simon Backx
* Added Milestone emails - Aileen Nowak
* Fixed broken link tracking in newsletters (#16473) - Chris Raible
* Fixed UI bug when choosing email recipients (#16481) - Chris Raible
* Fixed broken editor breadcrumbs when opening a new post from analytics (#16463) - Chris Raible

[4.40.1]
* Update Ghost to 5.40.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.40.1)
* Fixed member newsletter subscription not saving in Admin (#16490) - Rishabh Garg

[4.40.2]
* Update Ghost to 5.40.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.40.2)
* Fixed LinkReplacer bug causing broken links on published post/page (#16514) - Rishabh Garg

[4.41.0]
* Update Ghost to 5.41.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.41.0)
* Update Ghost CLI to 1.24.0
* Updated Casper to v5.4.8 - Ghost CI

[4.42.0]
* Update Ghost to 5.42.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.42.0)
* Added post analytics export - Simon Backx
* Improved scrollbar appearance in dark mode (#16535) - pavel
* Fixed member signup emails being sent with escaped subject line (#16544) - Chris Raible
* Fixed UI bug when selecting specific recipients by label (#16543) - Chris Raible

[4.42.1]
* Update Ghost to 5.42.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.42.1)
* Fixed path traversal issue in theme files - Naz

[4.42.2]
* Update Ghost to 5.42.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.42.2)

[4.42.3]
* Update Ghost to 5.42.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.42.3)

[4.43.0]
* Update Ghost to 5.43.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.43.0)
* Added signup terms to Portal - Simon Backx
* Updated Casper to v5.4.9 - Ghost CI

[4.44.0]
* Update Ghost to 5.44.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.44.0)
* Filter email only posts - Simon Backx

[4.45.0]
* Update Ghost to 5.45.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.45.1)
* Fixed images sometimes being stored as data: URLs when copy/pasting from other editors (#16707) - Kevin Ansfield
* Updated Casper to v5.4.10 - Ghost CI

[4.46.0]
* Update Ghost to 5.46.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.46.0)
* Added announcement bar setting - Elena Baidakova
* Added posts and pages bulk editing (#16634) - Simon Backx

[4.46.1]
* Update Ghost to 5.46.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.46.1)
* Fixed filtering on private Author fields in Content API (see advisory) - Fabien "egg" O'Carroll

[4.47.0]
* Update Ghost to 5.47.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.47.0)
* bug Added missing history logs for post/page bulk actions (#16734) - Simon Backx
* bug Improved error message for unauthorized YouTube embeds (#16374) - Chris Raible
* bug Fixed members breadcrumbs when not coming from analytics - Simon Backx

[4.47.1]
* Update Ghost to 5.47.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.47.1)
* Updated Casper to v5.4.11 - Ghost CI
* Fixed site setup hanging when mail isn't configured - Fabien 'egg' O'Carroll
* Fixed generating card assets with include allowlist (#16766) - Deepam Kapur
* Fixed Member signup when signup terms are null - Fabien "egg" O'Carroll

[4.47.2]
* Update Ghost to 5.47.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.47.2)
* Fixed visibility of the post history feature - Egg

[4.48.0]
* Update Ghost to 5.48.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.48.0)
* Implemented duplicate post functionality (#16767) - Michael Barrett
* Fixed trailing slash and space in HTML metadata elements (#16778) - Benjamin Rancourt
* Fixed validation error when creating 0% offer (#16803) - Michael Barrett
* Fixed link to translation information in general settings (#16804) - Michael Barrett
* Fixed outbound link tagger tagging non http urls (#16773) - Michael Barrett

[4.48.1]
* Update Ghost to 5.48.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.48.1)
* Fixed tiers not appearing on custom signup pages (#16828) - Kevin Ansfield

[4.49.0]
* Update Ghost to 5.49.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.49.0)
* Moved Substack migrator app to GA under beta features - Aileen Nowak
* Fixed issue where single letter product slugs cause 500 error (#16821) - Michael Barrett

[4.49.1]
* Update Ghost to 5.49.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.49.1)
* Fixed member activity event filter (#16849) - Michael Barrett

[4.49.2]
* Update Ghost to 5.49.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.49.2)

[4.49.3]
* Update Ghost to 5.49.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.49.3)

[4.50.0]
* Update Ghost to 5.50.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.50.0)
* Added beta of the new Ghost editor (#16923) - Kevin Ansfield

[4.50.1]
* Update Ghost to 5.50.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.50.2)
* Fixed copy/paste of HR cards between editor and beta editor - Kevin Ansfield
* Fixed existing snippets not being available in beta editor - Kevin Ansfield

[4.50.2]
* Update Ghost to 5.50.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.50.4)
* Fixed converter bug that stopped some snippets being migrated for the editor beta - Kevin Ansfield

[4.51.0]
* Update Ghost to 5.51.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.51.0)
* Added embeddable signup form - Simon Backx
* Fixed closing modals in admin UI when releasing mouse outside modal - Simon Backx

[4.51.1]
* Update Ghost to 5.51.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.51.1)

[4.51.2]
* Update Ghost to 5.51.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.51.2)
* Added word count to beta editor - Kevin Ansfield
* Fixed empty icon crashing signup embed preview (#17022) - Ronald Langeveld
* Fixed Post History for posts sent as an email (#16999) - Fabien 'egg' O'Carroll

[4.52.0]
* Update Ghost to 5.52.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.52.0)
* Added custom theme setting descriptions (#17024) - Michael Barrett
* Updated Casper to v5.4.11 - Ghost CI
* Reused attribution for embeddable signup forms on own site - Simon Backx
* Fixed signup form text wrapping after submitting - Simon Backx
* Fixed page revision returning empty array on update and restore (#17042) - Ronald Langeveld
* Fixed revisions relation not linked to pages api (#17037) - Ronald Langeveld
* Fixed visible scrollbars in signup form preview - Simon Backx
* Disabled signup form embed menu when members are disabled - Simon Backx
* Fixed horizontal scrolling on signup form input field. (#17002) - Ronald Langeveld
* Fixed snippet insertion in the new editor (#17003) - Elena Baidakova

[4.52.1]
* Update Ghost to 5.52.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.52.1)
* bug Fixed beta editor causing left-aligned images in some themes - Kevin Ansfield
* bug Fixed beta editor HTML cards auto-closing tags when rendering - Kevin Ansfield

[4.52.2]
* Update Ghost to 5.52.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.52.2)
* Fixed post scheduling for sites with transferred ownership (#17075) - Chris Raible

[4.52.3]
* Update Ghost to 5.52.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.52.3)
* Fixed revision save reverting newly published post to draft (#17083) - Michael Barrett

[4.53.0]
* Update Ghost to 5.53.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.53.0)
* Added signup card to beta editor (#17124) - https://ghost.org/changelog/editor-beta/ - Ronald Langeveld
* Updated Casper to v5.5.1 - Ghost CI
* Improved copy on page & post revisions (#17068) - Ronald Langeveld
* Improved copy on too many login attempts - Simon Backx
* Fixed newsletter post duplication (#17103) - Michael Barrett
* Handled BOM character for Unicode encoded file uploads (#17104) - Princi Vershwal
* Fixed portal free trial message incorrectly showing (#17095) - Michael Barrett
* Fixed revision save reverting newly published post to draft (#17083) - Michael Barrett
* Fixed draft pages not saving on forced revisions (#17081) - Ronald Langeveld
* Fixed accessibility issue with hidden success message in signup form - Simon Backx

[4.53.1]
* Update Ghost to 5.53.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.53.2)
* Fixed email breaking when signup card is added - Ronald Langeveld

[4.53.2]
* Update Ghost to 5.53.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.53.3)
* Fixed signup card in post plaintext and email preheader (#17163) - Simon Backx
* Fixed repeating text in plaintext version of emails (#17162) - Simon Backx
* Fixed members-only content incorrectly showing in plaintext email (#17137) - Michael Barrett

[4.53.3]
* Update Ghost to 5.53.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.53.4)

[4.54.0]
* Update Ghost to 5.54.0
* Update Ghost cli to 1.24.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.54.0)

[4.54.1]
* Update Ghost to 5.54.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.54.2)
* Fixed member links not added to autocomplete (#17327) - Ronald Langeveld
* Bumped minimum Node 16 version to 16.14.0 - Daniel Lockyer

[4.54.2]
* Update Ghost to 5.54.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.54.3)
* Fixed poor performance when rendering beta editor posts (#17388) - Kevin Ansfield
* Fixed memberlinks returning undefined in autocomplete. (#17385) - Ronald Langeveld

[4.54.3]
* Update Ghost to 5.54.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.54.4)
* Fixed ghost.min.css asset compilation - Daniel Lockyer

[4.55.0]
* Update Ghost to 5.55.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.55.0)
* Added support for importing Stripe Coupons as Offers (#17415) - Sag
* Added missing translation strings in portal (#17400) - Ronald Langeveld
* Fixed members unable to unsubscribe from plan if hidden in Portal (#17251) - Chris Raible
* Fixed success state on change password button. (#17410) - Ronald Langeveld

[4.55.1]
* Update Ghost to 5.55.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.55.1)
* Fixed image rendering in Outlook email client (#17475) - Chris Raible

[4.55.2]
* Update Ghost to 5.55.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.55.2)

[4.56.0]
* Update Ghost to 5.56.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.56.0)

[4.56.1]
* Update Ghost to 5.56.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.56.1)

[4.57.0]
* Update Ghost to 5.57.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.57.0)
* Updated Casper to v5.6.0 - Ghost CI
* Fixed email only to post rescheduling. (#17538) - Ronald Langeveld

[4.57.1]
* Update Ghost to 5.57.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.57.1)
* Fixed custom routing with collections (#17561) - Fabien 'egg' O'Carroll

[4.57.2]
* Update Ghost to 5.57.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.57.2)
* Fixed superfluous DB queries on Collection save - Fabien "egg" O'Carroll

[4.57.3]
* Update Ghost to 5.57.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.57.3)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[4.58.0]
* Update Ghost to 5.58.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.58.0)
* Fixed member filtering on newsletter subscription status (#17583) - Michael Barrett

[4.59.0]
* Update Ghost to 5.59.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.59.0)
* Added new enhanced Header Card (#17654) - Ronald Langeveld
* Added support for relative links in emails (#17630) - Simon Backx
* Fixed missing @page object in themes when rendering custom routed page (#17693) - Kevin Ansfield
* Fixed incorrect newsletter subscriber count on dashboard (#17683) - Michael Barrett
* Fixed overly small file drop area in beta editor (#17682) - Kevin Ansfield
* Fixed member filter by single newsletter subscription (#17668) - Michael Barrett
* Fixed member signup source attributed to Stripe (#17606) - Simon Backx

[4.59.1]
* Update Ghost to 5.59.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.59.1)
* Fixed arbitrary file read via symlinks in content import (see advisory) - Simon Backx
* Fixed newsletter filters not working in bulk operations (#17714) - Ronald Langeveld

[4.59.2]
* Update Ghost to 5.59.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.59.2)
* Fixed portal free signup link not working if free plan is hidden (#17742) - Chris Raible
* Fixed comments admin menu not visible when not logged in as member (#17749) - Simon Backx
* Fixed importing offers when importing members from Stripe (#17739) - Simon Backx
* Fixed Unsplash integration toggle in Koenig editor (#17725) - Ronald Langeveld
* Fixed newsletter filters not working in bulk operations (#17714) - Ronald Langeveld

[4.59.3]
* Update Ghost to 5.59.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.59.3)

[4.59.4]
* Update Ghost to 5.59.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.59.4)

[4.60.0]
* Update Ghost to 5.60.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.60.0)

[4.61.0]
* Update Ghost to 5.61.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.61.1)
* Added automatic conversion of old content when opening in beta editor (#17876) - Kevin Ansfield
* Fixed member newsletter filter when multiple filters applied (#17857) - Michael Barrett
* Fixed missing Admin assets in published tarball - Daniel Lockyer

[4.61.1]
* Update Ghost to 5.61.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.61.2)
* Fixed incorrect rendering of links following line breaks in beta editor - Kevin Ansfield

[4.61.2]
* Update Ghost to 5.61.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.61.3)
* Fixed bookmark card creation and pasted link unfurls (#17990) - Kevin Ansfield

[4.62.0]
* Update Ghost to 5.62.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.62.0)
* Added conversion to beta editor format when creating content via ?source=html (#18000) - Kevin Ansfield
* Fixed broken "< Posts" and "Back to editor" links in beta editor (infinite save loop) (#18042) - Kevin Ansfield
* Fixed bookmark card creation and pasted link unfurls (#17990) - Kevin Ansfield

[4.63.0]
* Update Ghost to 5.63.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.63.0)
* Fixed portal showing incorrect expiry date for comped subscription (#18120) - Michael Barrett
* Fixed incorrect order of author/publisher in bookmarks cards created with beta editor (#18085) - Kevin Ansfield

[4.64.0]
* Update Ghost to 5.64.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.64.0)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[4.65.0]
* Update Ghost to 5.65.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.65.0)
* nabled dynamic post rendering (#18183) - Steve Larson
* Fixed escaping search terms that contain special characters (#18151) - Mark Stosberg
* Fixed collection card post excerpts (#18232) - Steve Larson
* Repopulate featured collection records - Naz

[4.65.1]
* Update Ghost to 5.65.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.65.1)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[4.66.0]
* Update Ghost to 5.66.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.66.0)
* Fixed incorrect subscription status text when subscription is comped (#18369) - Michael Barrett
* Fixed issue with settings being marked as dirty when visibility is changed (#18370) - Michael Barrett
* Fixed staff emails eventually having invalid styles (#18363) - Simon Backx

[4.66.1]
* Update Ghost to 5.66.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.66.1)

[4.67.0]
* Update Ghost to 5.67.0
* Update Ghost CLI to 1.25.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.67.0)
* Added Source as the new default theme - Chris Raible
* Increased maximum number of custom theme settings to 20 - Daniel Lockyer
* Show specific tier posts under 'Paid-member only' filter in admin panel (#17043) - Princi Vershwal

[4.68.0]
* Update Ghost to 5.68.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.68.0)
* Released new editor
* Fixed bug preventing changes to tier benefit capitalization (#18406) - Chris Raible
* Fixed outboundLinkTagging setting affected whether member sources are tracked (#18498) - Simon Backx
* Fixed cache key generation not taking into account array value (#18476) - Michael Barrett

[4.69.0]
* Update Ghost to 5.69.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.69.0)
* Redesigned Admin settings (#18545) - Jono, Ronald & Zimo
* Updated Source to v1.0.1
* Fixed URL paste over selected text removing text rather than converting to link in Firefox (#18573) - Kevin Ansfield
* Fixed theme demo when multiple themes with variants (#18558) - Michael Barrett
* Fixed editor sometimes getting into a state that resulted in unexpected render output - Kevin Ansfield
* Fixed copy/paste from Word missing headings and text formats - Kevin Ansfield

[4.69.1]
* Update Ghost to 5.69.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.69.1)
* Fixed member search ignoring filters (#18600) - Chris Raible
* Fixed broken rendering and copy/paste when callout card has a bad bg color value (#18547) - renovate[bot]
* Fixed bug causing sites with many snippets to be rate limited (#18568) - Chris Raible

[4.69.2]
* Update Ghost to 5.69.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.69.2)
* Fixed styles in HTML card content interfering with editor display (#18647) - Kevin Ansfield
* Fixed potential invalid nesting of elements within heading nodes in editor (#18646) - Kevin Ansfield
* Fixed card following paragraph being deleted when pressing Delete at beginning of some formatted text - Kevin Ansfield

[4.69.3]
* Update Ghost to 5.69.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.69.3)

[4.69.4]
* Update Ghost to 5.69.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.69.4)
* Fixed free tier not updating in Portal settings (#18705) - Ronald Langeveld

[4.70.0]
* Update Ghost to 5.70.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.70.0)
* Updated Source to v1.0.2 - Ghost CI
* Improved search keywords in Admin-X Settings (#18608) - Jannis Fedoruk-Betschki
* Fixed Stripe modal not showing Account name (#18706) - Ronald Langeveld
* Fixed free tier not updating in Portal settings (#18705) - Ronald Langeveld
* Fixed plan upgrade not cancelling trial (#18699) - Michael Barrett
* Fixed backspace sometimes deleting a preceding card (e.g. backspace at end of link inside a paragraph preceded by a card)
* Fixed images sometimes not being visible when creating posts with ?source=html
* Fixed images wrapped in links losing their links when creating posts with ?source=html
* Fixed inline styles in HTML card content not displaying in the editor

[4.70.1]
* Update Ghost to 5.70.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.70.1)
* Fixed errors from using <br> in some situations when creating posts with ?source=html (#18714)

[4.70.2]
* Update Ghost to 5.70.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.70.2)
* Fixed custom setting image upload not working for multiple images

[4.71.0]
* Update Ghost to 5.71.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.71.0)
* Added Recommendations feature (#18743) - Sag
* Updated Source to v1.1.0 - Ghost CI
* Fixed custom setting image upload not working for multiple images (#18728) - Jono M
* Fixed indent behaviour for paragraphs (#18724) - renovate[bot]
* Dropped support for Node 16 - Daniel Lockyer

[4.71.1]
* Update Ghost to 5.71.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.71.1)
* Fixed mobiledoc and lexical content being available via Content API - Kevin Ansfield

[4.71.2]
* Update Ghost to 5.71.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.71.2)
* Fixed detection of `{{recommendations}}` and `{{readable_url}}` helpers - Daniel Lockyer

[4.72.0]
* Update Ghost to 5.72.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.72.0)
* Enabled emoji picker in various admin-x fields (#18850) - Steve Larson
* Added well-known assetlinks.json to allowed files - Hannah Wolfe
* Fixed showing recommendations on custom welcome pages (#18864) - Simon Backx
* Fixed adding recommendation with URL redirect breaking one-click-subscribe (#18863) - Simon Backx
* Fixed post feature image caption updates saving with every keystroke (#18833) - Steve Larson

[4.72.1]
* Update Ghost to 5.72.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.72.1)
* Fixed email rendering bug in Gmail for Android (#18886) - Chris Raible
* Fixed links being invisible in Portal settings (#18881) - Peter Zimon

[4.72.2]
* Update Ghost to 5.72.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.72.2)
* Fix edge case resulting in duplicate emails for some recipients

[4.73.0]
* Update Ghost to 5.73.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.73.0)
* Enabled emoji picker in editor (#18880) - Steve Larson
* Fix edge case resulting in duplicate emails for some recipients (#18941) - Steve Larson
* Fixed various editor issues (#18934) - Kevin Ansfield
* Fixed UI glitch on unsubscribe page in Portal - Simon Backx
* Fixed unsubscribe button requiring manual action + wrong confirmation - Simon Backx
* Changed "free_signups" to "signups" in Post Exporter (#18883) - Nick Moreton

[4.73.1]
* Update Ghost to 5.73.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.73.1)

[4.73.2]
* Update Ghost to 5.73.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.73.2)
* Fixed blank render output in some cases when using line breaks
* Fixed backspace at end of link sometimes deleting whole link in Firefox
* Fixed plain black generated video thumbnails in Safari
* Added srcset and loading="lazy" to header card images
* Improved accessibility of buttons in render output by adding aria-role attributes

[4.74.0]
* Update Ghost to 5.74.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.74.0)
* Added support for logging out members on all devices (#18935) - Simon Backx
* Added filter to filter members by email disabled - Simon Backx
* Added option to unsubscribe in one-click from emails (#19032) - Sag
* Updated Source to v1.1.1 - Ghost CI
* Better Polish translation, fixed grammar and typos (#18865) - Kacper Duras
* Fixed contain/starts/endsWith filters with /, _ or % in them (#19015) - Simon Backx
* Fixed highlight formatting not showing in rendered posts (#18997) - Kevin Ansfield
* Redirected email previews to /email/ route (#18976) - Steve Larson
* Fixed Enter key not working correctly when using IME in post title (#18950) - t8m8
* Improved GScan performance for themes with many partials - Daniel Lockyer

[4.74.1]
* Update Ghost to 5.74.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.74.1)
* Fixed re-ordering within galleries sometimes getting stuck on disabled

[4.74.2]
* Update Ghost to 5.74.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.74.2)
* Fixed creating posts with an empty root

[4.74.3]
* Update Ghost to 5.74.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.74.3)
* Fixed error when loading editor in Safari versions earlier than 16.4 - Kevin Ansfield

[4.74.4]
* Update Ghost to 5.74.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.74.4)
* Updated Source to v1.1.1 - Ghost CI
* Updated editor layout to be more mobile friendly (#19103) - Sanne de Vries
* Fixed About modal dev experiments info in Settings (#19087) - Ronald Langeveld

[4.74.5]
* Update Ghost to 5.74.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.74.5)
* Restricted reading files from outside the theme directory - Fabien "egg" O'Carroll

[4.75.0]
* Update Ghost to 5.75.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.75.0)
* Updated Source to v1.1.2 - Ghost CI
* Fixed slash not working in Koenig link editor (#19198) - Ronald Langeveld
* Fixed Portal default page handling - Simon Backx

[4.75.1]
* Update Ghost to 5.75.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.75.1)
* Fixed deleting members with email disabled (#19222) - Simon Backx

[4.75.2]
* Update Ghost to 5.75.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.75.2)
* Fixed editor crash when typing `:,`, `:|`, or similar - Kevin Ansfield

[4.75.3]
* Update Ghost to 5.75.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.75.3)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[4.76.0]
* Update Ghost to 5.76.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.76.0)
* Added TK Reminders feature (#19491) - Kevin Ansfield
* Updated editor layout to be more mobile friendly (#19327) - Sanne de Vries
* Fixed error logging crash when email recipients count if off by 1% (#19485) - Simon Backx
* Fixed outside click exiting Code Injection modal (#19424) - Ronald Langeveld
* Fixed XSS vulnerability involving post excerpts (#17190) - Joel DeSante
* Fixed hang in editor when back button is pressed whilst feature image caption is focused (#19367) - Kevin Ansfield
* Fixed callout card not rendering all inline formats (#19343) - Kevin Ansfield
* Fixed inability to drag-select text in caption alt inputs (#19342) - Kevin Ansfield
* Fixed quote and aside formatting being lost in single-block snippets (#19341) - Kevin Ansfield
* Fixed video uploads hanging in editor when using iOS (#19302) - Kevin Ansfield
* Fixed links in signup terms (#19235) - Sag

[4.76.1]
* Update Ghost to 5.76.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.76.1)
* Fixed externally hosted images overflowing in Outlook (#19527) - Chris Raible
* Fixed rare rendering issue of lists appearing as headings (#19511) - Kevin Ansfield

[4.76.2]
* Update Ghost to 5.76.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.76.2)
* Fixed custom excerpts sometimes being cut off (#19560) - Steve Larson
* Fixed in-editor style regressions (#19558) - Kevin Ansfield
* Fixed links in History (#19551) - Ronald Langeveld
* Fixed `primary_name` crashing Settings history (#19550) - Ronald Langeveld

[4.77.0]
* Update Ghost to 5.77.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.77.0)
* Improved Portal and Portal settings (#19584) - Sag
* Moved Offers to the new settings (#19493) - Ronald Langeveld
* Fixed broken access to preview of scheduled email-only posts (#19539) - Kevin Ansfield
* Fixed externally hosted images overflowing in Outlook (#19527) - Chris Raible
* Fixed wording when a canceled subscription has not ended yet (#19523) - Sag
* Fixed embed service trying http before https for oembed providers (#19521) - Kevin Ansfield
* Fixed rare rendering issue of lists appearing as headings (#19511) - renovate[bot]
* Fixed searching posts in member filters (#19505) - Simon Backx

[4.78.0]
* Update Ghost to 5.78.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.78.0)
* Improved sending email addresses for self-hosters (#19617) - Simon Backx

[4.79.0]
* Update Ghost to 5.79.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.79.0)
* Fixed trailing slashes not handled in Settings (#19640) - Ronald Langeveld
* Fixed members events for archived newsletters (#19638) - Ronald Langeveld
* Fixed embed card "Paste URL as link" button (#19634) - Kevin Ansfield
* Increased email timeout when sending to lots of recipients (#19628) - Simon Backx
* Fixed error when converting or pasting HTML content with headings+text nested inside lists (#19618) - Kevin Ansfield

[4.79.1]
* Update Ghost to 5.79.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.79.1)
* Fixed duplicate text when pasting URL on selection with multiple formats (#19671) - Kevin Ansfield
* Fixed members importer overwriting name and note if left blank (#19663) - Chris Raible
* Fixed members import unsubscribing members when subscribe_to_emails is empty (#19658) - Chris Raible

[4.79.2]
* Update Ghost to 5.79.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.79.3)
* Fixed editor card panels being positioned under post settings menu (#19635) - renovate[bot]
* Fixed editor feature image caption not expanding to show long content (#19688) - Kevin Ansfield

[4.79.3]
* Update Ghost to 5.79.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.79.4)
* Fixed wide + full-width cards not displaying correctly inside the editor (#19711) - Kevin Ansfield

[4.79.4]
* Update Ghost to 5.79.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.79.5)
* Added option to change the name of the free tier (#19715) - Sag
* Fixed explicit HTML entities being decoded when rendering HTML cards (#19728) - Kevin Ansfield
* Fixed iFrame not keeping scroll position on update (#19725) - Ronald Langeveld
* Fixed Portal icon misaligned in Preview (#19723) - Ronald Langeveld
* Fixed custom theme textfield rerendering (#19714) - Ronald Langeveld
* Fixed wide + full-width cards not displaying correctly inside the editor (#19711) - Kevin Ansfield

[4.79.5]
* Update Ghost to 5.79.6
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.79.6)

[4.80.0]
* Update Ghost to 5.80.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.80.0)
* Added lazy-loading of comments data (#19778) - Kevin Ansfield
* Include archived offers in members filter (#19756) - Ronald Langeveld
* Prevented newsletter subscriptions from getting out of sync in Portal (#19768) - Steve Larson
* Fixed cache invalidation header race conditions - Fabien O'Carroll
* Fixed extra whitespace in plaintext transactional member emails (#19736) - Kevin Ansfield
* Fixed pasting into HTML card editor replacing the card with a paragraph (#19757) - Kevin Ansfield

[4.80.1]
* Update Ghost to 5.80.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.80.1)
* Improved lazy-loading of comments data (#19809) - Kevin Ansfield
* Fixed free tier showing in the tiers-only paywall in posts (#19807) - Sag

[4.80.2]
* Update Ghost to 5.80.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.80.2)
* Updated Source to v1.2.0 - Ghost CI
* Improved lazy-loading of comments data (#19809) - Kevin Ansfield
* Fixed free tier showing in the tiers-only paywall in posts (#19807) - Sag
* Fixed unexpected conversion of single-quoted attributes in HTML cards (#19727) - Kevin Ansfield
* Fixed 500 error for premature api token use - Fabien O'Carroll
* Fixed emojis not updating in Announcement Bar (#19792) - Ronald Langeveld

[4.80.3]
* Update Ghost to 5.80.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.80.4)
* Updated Source to v1.2.1 - Ghost CI
* Updated Casper to v5.7.1 - Ghost CI
* Reduced requests and 403 responses for comments auth check (#19840) - Kevin Ansfield
* Fixed 500 errors for invalid theme layouts (#19848) - Fabien 'egg' O'Carroll
* Fixed /p/ redirects not being indexed by search engines (#19864) - Michael Barrett
* Fixed adding recommendation when oembed fails (#19861) - Sag
* Fixed comments block disappearing when performing certain actions (#19846) - Kevin Ansfield

[4.80.4]
* Update Ghost to 5.80.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.80.5)

[4.81.0]
* Update Ghost to 5.81.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.81.0)
* Fixed missing source + resized images producing rendered 404 (#19869) - Daniel Lockyer

[4.81.1]
* Update Ghost to 5.81.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.81.1)
* Fixed adding recommendations with long excerpts (#19949) - Sag
* Fixed keeping existing attribution in recommendations (#19945) - Sag

[4.82.0]
* Update Ghost to 5.82.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.0)
* Added escaping to member export CSV fields - Daniel Lockyer
* Fixed jerky scrolling in Site Design for Safari (#19974) - Ronald Langeveld

[4.82.1]
* Update Ghost to 5.82.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.1)
* Improved post-setup onboarding flow (#19996) - Kevin Ansfield
* Fixed Gscan type error crashing settings (#19994) - Ronald Langeveld

[4.82.2]
* Update Ghost to 5.82.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.2)
* Updated Source to v1.2.2 - Ghost CI
* Updated Casper to v5.7.2 - Ghost CI
* Fixed First Promoter always showing Active (#20010) - Ronald Langeveld

[4.82.3]
* Update Ghost to 5.82.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.3)
* Changed TK reminders to be case-insensitive (#20024) - Kevin Ansfield
* Fixed admin error when deleting an unsaved or imported post (#20053) - Chris Raible
* Fixed file card button not being linked in emails (#20023) - renovate[bot]
* Added support for Node 20 - please report any issues with Node 20 support to GitHub.

[4.82.4]
* Update Ghost to 5.82.4
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.4)

[4.82.5]
* Update Ghost to 5.82.5
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.5)

[4.82.6]
* Update Ghost to 5.82.6
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.6)
* update i18n for better fit (pt-br) (#20045) - Sérgio Spagnuolo

[4.82.7]
* Update Ghost to 5.82.7
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.7)
* Fixed Admin search sometimes stalling on first query (#20143) - Kevin Ansfield

[4.82.8]
* Update Ghost to 5.82.8
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.8)
* Fixed updating payment method when beta flag is on (#20171) - Sag

[4.82.9]
* Update Ghost to 5.82.9
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.9)
* Fixed error rendering when using a duplicate offer code (#20156) - Sag
* Fixed Admin search sometimes stalling on first query (#20143) - Kevin Ansfield

[4.82.10]
* Update Ghost to 5.82.10
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.10)
* Fixed direct paid signups on Stripe beta (#20215) - Ronald Langeveld

[4.82.11]
* Update Ghost to 5.82.11
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.11)
* Fixed certain snippets not inserting correctly (#20129) - renovate[bot]
* Fixed direct paid signups on Stripe beta (#20215) - Ronald Langeveld

[4.82.12]
* Update Ghost to 5.82.12
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.82.12)
* Fixed External Image URLs being incorrectly prefixed (#20226) - Ronald Langeveld
* Fixed redundant member data loading for static assets (#20031) - Ronald Langeveld

[4.83.0]
* Update Ghost to 5.83.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.83.0)
* Added internal linking beta - Kevin Ansfield
* Fixed admin error when trying to overwrite a default theme (#20299) - Chris Raible

[4.84.0]
* Update Ghost to 5.84.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.84.0)
* Added beta feature toggle for moving excerpt field into editor (#20341) - Kevin Ansfield
* Added newsletter design setting to display excerpt as subtitle - Kevin Ansfield
* Updated Source to v1.2.3 - Ghost CI
* Fixed default sort for the content API posts endpoint with included relations (#20333) - Steve Larson
* Fixed Slack integration using member content in excerpt (#20328) - Michael Barrett
* Fixed mailto: not being recognised in internal-linking search (#20331) - renovate[bot]

[4.84.1]
* Update Ghost to 5.84.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.84.1)

[4.84.2]
* Update Ghost to 5.84.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.84.2)

[4.85.0]
* Update Ghost to 5.85.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.85.0)

[4.85.1]
* Update Ghost to 5.85.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.85.1)

[4.85.2]
* Update Ghost to 5.85.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.85.2)

[4.86.1]
* Update Ghost to 5.86.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.86.1)

[4.86.2]
* Update Ghost to 5.86.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.86.2)

[4.87.0]
* Update Ghost to 5.87.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.87.0)

[4.87.1]
* Update Ghost to 5.87.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.87.1)

[4.87.2]
* Update Ghost to 5.87.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.87.2)

[4.87.3]
* Update Ghost to 5.87.3
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.87.3)

[4.88.0]
* Update Ghost to 5.88.0
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.88.0)
* Improved performance loading the posts list in admin (#20618) - Steve Larson
* Updated Source to v1.3.2 - Ghost CI
* Added staff notification when a sub is canceled due to failed payments (#20534) - Sag
* Fixed admin crashing when deleting a tier benefit that was last moved (#20628) - Steve Larson
* Fixed publishing issue when site has no active newsletters (#20627) - Princi Vershwal
* Fixed member subscription details in Admin (#20619) - Sag

[4.88.1]
* Update Ghost to 5.88.1
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.88.1)
* Fixed listing pages in Admin (#20633) - Kevin Ansfield

[4.88.2]
* Update Ghost to 5.88.2
* [Full changelog](https://github.com/TryGhost/Ghost/releases/tag/v5.88.2)

[4.88.3]
* update ghost to 5.88.3
* [full changelog](https://github.com/tryghost/ghost/releases/tag/v5.88.3)

[4.89.0]
* update ghost to 5.89.0
* [full changelog](https://github.com/tryghost/ghost/releases/tag/v5.89.0)

[4.89.1]
* Update ghost to 5.89.1
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.89.1)
* Fixed Content-Type for RSS feed (#20670) - Steffo
* Fixed unsaved changes confirmation on Lexical schema change (#20687) - Ronald Langeveld

[4.89.2]
* Update ghost to 5.89.2
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.89.2)
* Added “Copy post link” to posts list context menu (#20760) - Sanne de Vries
* Removed member bulk deletion safeguard from safe queries (#20747) - Sag
* Fixed additional white space appearing at bottom of editor (#20757) - Ronald Langeveld
* Fixed newsletter button not hidden in Portal (#20732) - Ronald Langeveld

[4.89.3]
* Update ghost to 5.89.3
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.89.3)
* Fixed a bug causing new drafts to only save if the title is populated (#20769) - Chris Raible

[4.89.4]
* Update ghost to 5.89.4
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.89.4)
* Fixed editor unsaved changes modal showing too often (#20787) - Kevin Ansfield
* Fixed autosave not triggering when in-editor excerpt is changed (#20785) - Kevin Ansfield

[4.89.5]
* Update ghost to 5.89.5
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.89.5)

[4.89.6]
* Update ghost to 5.89.6
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.89.6)

[4.90.0]
* Update ghost to 5.90.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.90.0)

[4.90.1]
* Update ghost to 5.90.1
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.90.1)

[4.90.2]
* Update ghost to 5.90.2
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.90.2)

[4.91.0]
* Update ghost to 5.91.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.91.0)
* Improved publishing flow (#20878) - Sodbileg Gansukh
* Added integrity token to one click subscribe (#20836) - Steve Larson

[4.92.0]
* Update ghost to 5.93.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.93.0)

[4.93.0]
* Update ghost to 5.94.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.94.0)
* Fixed "Unsaved changes" modal showing for some published posts with images - Kevin Ansfield
* Fixed bookmark card hot linking icons and thumbnails (#20923) - Princi Vershwal
* Fixed bookmark card hot linking icons and thumbnails ENG-904 (#20906) - Princi Vershwal

[4.93.1]
* Update ghost to 5.94.1
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.94.1)

[4.93.2]
* Update ghost to 5.94.2
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.94.2)
* wrapped missing i18n strings in portal (#21042) - Cathy Sarisky
* Added a maximum limit of 100 for GET members admin endpoint (#20643) - Princi Vershwal
* Fixed wrong breadcrumb and missing post status when starting new post from analytics - Kevin Ansfield
* Fixed fetching and storing bookmark card icons and thumbnails (#21036) - Princi Vershwal
* Fixed member filtering for "Unsubscribed from newsletter" filters (#20926) - Sag

[4.94.0]
* Update ghost to 5.95.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.95.0)
* Added i18n support to search - ready for translator work! (#21055) - Cathy Sarisky
* Updated Casper to v5.7.5 - Ghost CI
* Added missing locales to i18n.js (#21144) - Cathy Sarisky
* Fixed infinite loops in setFeatureImageCaption for deleted posts (#21081) - Ronald Langeveld
* Fixed navigations links for Ghost sites hosted on a subdirectory (#21071) - Sag
* Added Estonian translations (#21129) - WindTheWhisperer
* Added new Brazilian Portuguese new i18n strings (#21101) - Sérgio Spagnuolo
* Updated Turkish translation (#21110) - Mertcan GÖKGÖZ
* Added Bulgarian translation for search (#21102) - Stanislav Traykov
* Bulgarian translation updates (Sep 2024) (#21062) - Yovko Lambrev
* German translations (#21050) - Cathy Sarisky
* Updated Gaelic portal / search translations (#21093) - Chris Mitchell
* Update pt-BR translation of search.json (#21100) - Alexandre "Lekler" Rodrigues

[4.95.0]
* Update ghost to 5.96.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.96.0)
* Added `content_api_key` helper (#21151) - Cathy Sarisky
* Fixed errors creating new posts after a post access change via context menu - Kevin Ansfield
* Fixed capitalization in Spanish sign-up string (#20711) - Fernando Ochoa Olivares
* Fixed missing Greek strings (#21137) - Cathy Sarisky
* Updated packages for improved i18n support (#21218) - Steve Larson
* Updated Hindi translations (#21208) - Cathy Sarisky
* Added Urdu translations (#21209) - Cathy Sarisky
* Updated Norwegian translations (#20917) - matsbst
* Added Kazakh locale for Portal (#20698) - ayangizzat
* Improved i18n support for Portal error messages (#21190) - Cathy Sarisky
* Added Bahasa Indonesia translation for search & portal (#21179) - Raka Afp
* Added German translations for search (#21163) - Leif Singer

[4.95.1]
* Update ghost to 5.96.1
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.96.1)
* Fixed 404 error for Tips & Donations on subdirectory sites (#​21250) - Cathy Sarisky
* Improved performance of several checks in GScan - Daniel Lockyer
* Updated Vietnamese translation (#​21230) - Duy
* Updated pt-BR translations of portal.json (#​21244) - Alexandre "Lekler" Rodrigues
* Updated Spanish translations (#​21232) - Fernando Ochoa
* Added new Ukrainian translations and improved existing ones (#​21235) - Volodymyr Lavrynovych
* Added locale for Bengali Language (bn) (#​20432) - Shah Newaj
* Updated Vietnamese translation (#​21227) - Duy
* Added Bahasa Indonesia translation for Portal error messages (#​21224) - Raka Afp
* Added new German translations to Portal (#​21223) - Jannis Fedoruk-Betschki
* Added Swahili language translations (#​20485) - Peter Jidamva
* Added a few missing strings for Croatian i18n (#​20449) - Bobve

[4.95.2]
* Update ghost to 5.96.2
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.96.2)
* Fixed missing incoming recommendations (#21317) - Sag

[4.96.0]
* Update ghost to 5.97.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.97.0)
* Added unsubscribe_url to member api response (#​21207) - Steve Larson
* Fixed NQL filters for single letter slugs (#​21340) - Steve Larson
* Fixed missing incoming recommendations (#​21317) - Sag
* Fixed missing 'duplicate a post' feature for editors (#​21304) - Sag
* New translations and bug fix on /month and /year (#​21267) - Cathy Sarisky

[4.96.1]
* Update ghost to 5.97.1
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.97.1)
* Fixed hidden comments still appearing - Fabien 'egg' O'Carroll

[4.96.2]
* Update ghost to 5.97.2
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.97.2)

[4.96.3]
* Update ghost to 5.97.3
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.97.3)

[4.97.0]
* Update ghost to 5.98.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.98.0)
* Added `content_api_url` helper (#21331) - Cathy Sarisky
* Updated Source to v1.3.3 - Ghost CI
* Supported RTL languages in Portal - Cathy Sarisky
* Fixed recommendations popup not opening - Cathy Sarisky
* Fixed hidden comments still appearing - Fabien 'egg' O'Carroll
* fixed routing error when no recommendations (#21251) - Cathy Sarisky

[4.97.1]
* Update ghost to 5.98.1
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.98.1)
* Fixed malformed `unsubscribe_url` in members api response (#21437) - Steve Larson


[4.98.0]
* Update ghost to 5.99.0
* [Full changelog](https://github.com/tryghost/ghost/releases/tag/v5.99.0)


[4.99.0]
* Update Ghost to 5.100.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.100.0)
* Added i18n for portal, newsletters, search, comments, etc ([#&#8203;21547](https://github.com/tryghost/ghost/issues/21547)) - Hannah Wolfe
* Updated Portal to accept data-locale ([#&#8203;21420](https://github.com/tryghost/ghost/issues/21420)) - Cathy Sarisky
* Fixed translations not being picked up in newsletters without reboot ([#&#8203;21549](https://github.com/tryghost/ghost/issues/21549)) - Cathy Sarisky
* Fixed error message for when 2fa email sending fails ([#&#8203;21541](https://github.com/tryghost/ghost/issues/21541)) - Princi Vershwal
* Fixed migrations for SQLite database users ([#&#8203;19839](https://github.com/tryghost/ghost/issues/19839)) ([#&#8203;21063](https://github.com/tryghost/ghost/issues/21063)) - Mark Stosberg
* Fixed "Unsaved post" modal shown when updating visibility in post settings ([#&#8203;21511](https://github.com/tryghost/ghost/issues/21511)) - Princi Vershwal
* Fixed post not being saved when updating tags ([#&#8203;21503](https://github.com/tryghost/ghost/issues/21503)) - Princi Vershwal
* Updated swedish translations ([#&#8203;21562](https://github.com/tryghost/ghost/issues/21562)) - Daniel Sjberg
* Updated Romanian translations ([#&#8203;21550](https://github.com/tryghost/ghost/issues/21550)) - Andrei Hodorog
* Updated Turkish translations ([#&#8203;21553](https://github.com/tryghost/ghost/issues/21553)) - echobilisim3421
* Added Polish translations for search ([#&#8203;21548](https://github.com/tryghost/ghost/issues/21548)) - mafizaki
* Added more Italian translations ([#&#8203;21535](https://github.com/tryghost/ghost/issues/21535)) - MonRyse
* Updated Romanian (RO) translation strings ([#&#8203;21542](https://github.com/tryghost/ghost/issues/21542)) - Andrei Hodorog
* Updated Bulgarian translations ([#&#8203;21533](https://github.com/tryghost/ghost/issues/21533)) - Yovko Lambrev
* Added more zh-Hant translations ([#&#8203;21502](https://github.com/tryghost/ghost/issues/21502)) - Gary Lai

[4.99.1]
* Update Ghost to 5.100.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.100.1)
* Fixed newsletter not sending if locale is invalid ([#&#8203;21573](https://github.com/tryghost/ghost/issues/21573)) - Hannah Wolfe

[4.100.0]
* Update ghost to 5.101.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.101.0)
* Added custom fonts ([#&#8203;21564](https://github.com/tryghost/ghost/issues/21564)) - Sodbileg Gansukh
* Updated Casper to v5.8.0 - Ghost CI
* Updated Source to v1.4.0 - Ghost CI
* Fixed marketplace theme installation route ([#&#8203;21616](https://github.com/tryghost/ghost/issues/21616)) - Ronald Langeveld
* Fixed Admin navigation settings, pressing ENTER will now create a new navigation item ([#&#8203;21591](https://github.com/tryghost/ghost/issues/21591)) - Princi Vershwal
* Fixed newsletter not sending if locale is invalid ([#&#8203;21573](https://github.com/tryghost/ghost/issues/21573)) - Hannah Wolfe
* Translated newsletter.json to Norwegian ([#&#8203;21595](https://github.com/tryghost/ghost/issues/21595)) - matsbst
* Added Korean i18n translations ([#&#8203;21577](https://github.com/tryghost/ghost/issues/21577)) - Sunghyun Kim
* Added Dutch (nl) locale to newsletter.json ([#&#8203;21588](https://github.com/tryghost/ghost/issues/21588)) - jubi-git
* Updated Danish (dk) Translations ([#&#8203;21593](https://github.com/tryghost/ghost/issues/21593)) - Christian Schou
* Improved French translations ([#&#8203;21570](https://github.com/tryghost/ghost/issues/21570)) - Bastien
* Updated French translations ([#&#8203;21498](https://github.com/tryghost/ghost/issues/21498)) - Bastien

[4.100.1]
* Update ghost to 5.101.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.101.1)
* Added Arabic locale ([#&#8203;21590](https://github.com/tryghost/ghost/issues/21590)) - Mostafa Elsayed Hussin
* Added Hebrew translations ([#&#8203;17056](https://github.com/tryghost/ghost/issues/17056)) - Oren Cohen
* Updated Vietnamese translations ([#&#8203;21581](https://github.com/tryghost/ghost/issues/21581)) - Duy
* Added Czech translations for newsletter & search ([#&#8203;21516](https://github.com/tryghost/ghost/issues/21516)) - Lukas Kocourek
* Added Nepali locale for Ghost ([#&#8203;21552](https://github.com/tryghost/ghost/issues/21552)) - Sparsh
* Added pt-BR translations ([#&#8203;21612](https://github.com/tryghost/ghost/issues/21612)) - Fernanda Thiesen

[4.100.2]
* Update ghost to 5.101.2
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.101.2)

[4.100.3]
* Update ghost to 5.101.3
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.101.3)

[4.100.4]
* Update ghost to 5.101.4
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.101.4)
* Fixed unsubscribe all link in Portal sometimes failing ([#&#8203;21703](https://github.com/tryghost/ghost/issues/21703)) - Steve Larson
* Fixed slug not always updating for draft posts ([#&#8203;21691](https://github.com/tryghost/ghost/issues/21691)) - Steve Larson
* fix Windows bug with the admin file upload widget ([#&#8203;21687](https://github.com/tryghost/ghost/issues/21687)) - Cathy Sarisky
* Fixed Istanbul timezone ([#&#8203;21686](https://github.com/tryghost/ghost/issues/21686)) - Steve Larson
* Fixed missing progress indicator when submitting comments - Kevin Ansfield
* Fixed minifier for Windows users ([#&#8203;21311](https://github.com/tryghost/ghost/issues/21311)) - Steve Larson
* Fixed scheduled post datepicker marking prior day as selected ([#&#8203;21657](https://github.com/tryghost/ghost/issues/21657)) - Steve Larson
* Fixed words breaking to new line in emails on mobile ([#&#8203;21652](https://github.com/tryghost/ghost/issues/21652)) - Princi Vershwal
* Fixed avatar on comment reply form not showing your avatar - Kevin Ansfield
* Updated pt-BR translations ([#&#8203;21636](https://github.com/tryghost/ghost/issues/21636)) - Fernanda Thiesen
* Updated Turkish translations in comments.json ([#&#8203;21670](https://github.com/tryghost/ghost/issues/21670)) - echobilisim3421
* Added Nepali locale for newsletter.json ([#&#8203;21635](https://github.com/tryghost/ghost/issues/21635)) - Sparsh
* Added Arabic locale ([#&#8203;21590](https://github.com/tryghost/ghost/issues/21590)) - Mostafa Elsayed Hussin
* Added Hebrew translations ([#&#8203;17056](https://github.com/tryghost/ghost/issues/17056)) - Oren Cohen
* Updated Vietnamese translations ([#&#8203;21581](https://github.com/tryghost/ghost/issues/21581)) - Duy
* Added Czech translations for newsletter & search ([#&#8203;21516](https://github.com/tryghost/ghost/issues/21516)) - Lukas Kocourek
* Added Nepali locale for Ghost ([#&#8203;21552](https://github.com/tryghost/ghost/issues/21552)) - Sparsh
* Added pt-BR translations ([#&#8203;21612](https://github.com/tryghost/ghost/issues/21612)) - Fernanda Thiesen

[4.100.5]
* Update ghost to 5.101.5
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.101.5)

[4.100.6]
* Update ghost to 5.101.6
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.101.6)
* Fixed unclear newsletter (un)subscribe modal ([#&#8203;21739](https://github.com/tryghost/ghost/issues/21739)) - Danil van der Winden
* Fixed triggering wrong error for invalid client versions - Daniel Lockyer
* Updated Vietnamese translations ([#&#8203;21726](https://github.com/tryghost/ghost/issues/21726)) - ng Ngc Qung
* Bulgarian translation - ongoing updates and fixes ([#&#8203;21719](https://github.com/tryghost/ghost/issues/21719)) - Yovko Lambrev

[4.101.0]
* Update ghost to 5.103.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.103.0)
* Fixed feedback buttons' layout breaking in different languages, in newsletters ([#&#8203;21752](https://github.com/tryghost/ghost/issues/21752)) - Danil van der Winden
* Removed UI for AMP ([#&#8203;21762](https://github.com/tryghost/ghost/issues/21762)) - Hannah Wolfe

[4.102.0]
* Update ghost to 5.104.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.104.0)
* Fixed SVG sanitization for staff profile pictures ([#&#8203;21798](https://github.com/tryghost/ghost/issues/21798)) - Sag
* Added custom font support for themes ([#&#8203;21817](https://github.com/tryghost/ghost/issues/21817)) - Steve Larson
* Updated Casper to v5.8.1 - Ghost CI
* Updated Source to v1.4.1 - Ghost CI

[4.102.1]
* Update ghost to 5.104.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.104.1)

[4.102.2]
* Update ghost to 5.104.2
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.104.2)

[4.103.0]
* Update ghost to 5.105.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.105.0)
* Limited permissions for uploaded files to 0644 ([#&#8203;21841](https://github.com/tryghost/ghost/issues/21841)) - Princi Vershwal
* Improved various aspects of comments app - Kevin Ansfield
* Fixed responsive issues with Posts filters ([#&#8203;21871](https://github.com/tryghost/ghost/issues/21871)) - Danil van der Winden
* Fixed mobile navigation for Admin ([#&#8203;21863](https://github.com/tryghost/ghost/issues/21863)) - Danil van der Winden
* Fixed missing subscription attribution on free to paid upgrade ([#&#8203;21846](https://github.com/tryghost/ghost/issues/21846)) - Sag
* Updated Catalan translations in comments.json ([#&#8203;21827](https://github.com/tryghost/ghost/issues/21827)) - lex Rodrguez Bacardit
* Added missing Turkish translations to portal.json ([#&#8203;21784](https://github.com/tryghost/ghost/issues/21784)) - echobilisim3421
* Updated Catalan translations in signup-form.json ([#&#8203;21831](https://github.com/tryghost/ghost/issues/21831)) - lex Rodrguez Bacardit

[4.104.0]
* Update ghost to 5.106.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.106.1)
* Added support for line breaks in more editor card fields (credit: [@&#8203;cathysarisky](https://github.com/cathysarisky)) - renovate\[bot]
* Added option to disable free signups ([#&#8203;21862](https://github.com/tryghost/ghost/issues/21862)) - Sag
* Made 5 settings quicker to edit at the top-level in Settings ([#&#8203;21976](https://github.com/tryghost/ghost/issues/21976)) - Danil van der Winden
* Improved Access card layout in Settings ([#&#8203;21920](https://github.com/tryghost/ghost/issues/21920)) - Danil van der Winden
* Improved Access card layout in Settings ([#&#8203;21913](https://github.com/tryghost/ghost/issues/21913)) - Danil van der Winden
* Fixed post list not displaying correctly with long titles ([#&#8203;21892](https://github.com/tryghost/ghost/issues/21892)) - Danil van der Winden
* Fixed archived newsletters visible in Portal when email disabled ([#&#8203;21737](https://github.com/tryghost/ghost/issues/21737)) - Ronald Langeveld
* Updated Italian translations ([#&#8203;21905](https://github.com/tryghost/ghost/issues/21905)) - Alessandro Massone

[4.105.0]
* Update ghost to 5.107.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.107.0)
* Updated Comments likes UI changes to be instant ([#&#8203;21861](https://github.com/tryghost/ghost/issues/21861)) - Ronald Langeveld
* Optimised SQL query for exporting members ([#&#8203;22017](https://github.com/tryghost/ghost/issues/22017)) - Princi Vershwal
* Admin visual design updates ([#&#8203;21987](https://github.com/tryghost/ghost/issues/21987)) - Peter Zimon
* Fixed custom view not showing edit button in bad state ([#&#8203;22002](https://github.com/tryghost/ghost/issues/22002)) - Ronald Langeveld

[4.105.1]
* Update ghost to 5.107.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.107.1)
* Blocked spammy email domains in member signups ([#&#8203;22027](https://github.com/tryghost/ghost/issues/22027)) - Sag

[4.105.2]
* Update ghost to 5.107.2
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.107.2)
* Fixed degraded database performance when using the Post Analytics screen ([#&#8203;22031](https://github.com/tryghost/ghost/issues/22031)) - Chris Raible

[4.106.0]
* Update ghost to 5.108.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.108.1)
* Blocked spammy email domains in member signups ([#&#8203;22027](https://github.com/tryghost/ghost/issues/22027)) - Sag
* Fixed tags and authors not fitting in the input field ([#&#8203;22052](https://github.com/tryghost/ghost/issues/22052)) - Danil van der Winden
* Fixed newsletters not rendering in Portal Email Preferences ([#&#8203;22037](https://github.com/tryghost/ghost/issues/22037)) - Chris Raible
* Fixed degraded database performance when using the Post Analytics screen ([#&#8203;22031](https://github.com/tryghost/ghost/issues/22031)) - Chris Raible

[4.106.1]
* Update ghost to 5.108.2
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.108.2)
* This release contains fixes for minor bugs and issues reported by Ghost users.

[4.107.0]
* Update ghost to 5.109.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.109.0)
* Made names for uploaded files more secure and resilient to filesystems' limits ([#&#8203;22055](https://github.com/tryghost/ghost/issues/22055)) - Sag
* Enabled publishers to block additional email domains in member signups ([#&#8203;22047](https://github.com/tryghost/ghost/issues/22047)) - Sag
* Fixes dropdowns in Members filters ([#&#8203;22082](https://github.com/tryghost/ghost/issues/22082)) - Danil van der Winden
* Updated Norwegian translations ([#&#8203;21958](https://github.com/tryghost/ghost/issues/21958)) - Audun
* Updated Spanish translations ([#&#8203;21956](https://github.com/tryghost/ghost/issues/21956)) - .hj barraza
* Added missing Scottish Gaelic translations ([#&#8203;21979](https://github.com/tryghost/ghost/issues/21979)) - Chris Mitchell
* Updated Bulgarian translations ([#&#8203;21932](https://github.com/tryghost/ghost/issues/21932)) - Yovko Lambrev
* Added and improved Dutch translations ([#&#8203;21959](https://github.com/tryghost/ghost/issues/21959)) - R

[4.107.1]
* Update ghost to 5.109.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.109.1)

[4.107.2]
* Update ghost to 5.109.2
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.109.2)

[4.107.3]
* Update ghost to 5.109.3
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.109.3)
* Removed stdout from default production logging options - Ricardo Pinto
* Updated Slovenian translations - martinverbic
* Updated Catalan transaltions - lex Rodrguez Bacardit
* Updated Brazilian Portuguese translations - Mateus Ribeiro
* Updated Vietnamese translations - Duy
* Updated Turkish translations - echobilisim3421

[4.107.4]
* Update ghost to 5.109.4
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.109.4)

[4.107.5]
* Update ghost-cli to 1.27.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.109.4)

[4.107.6]
* Update ghost to 5.109.6
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.109.4)
* Fixed invalid `routes.yaml` filter preventing Ghost from booting ([#&#8203;22174](https://github.com/tryghost/ghost/issues/22174)) - Michael Barrett
* Updated Catalan translations ([#&#8203;21826](https://github.com/tryghost/ghost/issues/21826)) - lex Rodrguez Bacardit
* Updated Catalan translations ([#&#8203;21829](https://github.com/tryghost/ghost/issues/21829)) - lex Rodrguez Bacardit
* Updated Bulgarian translations ([#&#8203;22097](https://github.com/tryghost/ghost/issues/22097)) - Yovko Lambrev

[4.108.0]
* Update ghost to 5.110.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.110.0)
* Added support for Node 22 - Daniel Lockyer
* Fixed missing attribution in new member and new subscription staff emails ([#&#8203;22252](https://github.com/tryghost/ghost/issues/22252)) - Steve Larson
* Fixed subscription attribution sometimes missing from `SubscriptionCreatedEvent` ([#&#8203;22219](https://github.com/tryghost/ghost/issues/22219)) - Steve Larson
* Updated Slovak translations ([#&#8203;21800](https://github.com/tryghost/ghost/issues/21800)) - Techkrypt-xyz
* Added missing Russian translations ([#&#8203;22186](https://github.com/tryghost/ghost/issues/22186)) - Paul

[4.108.1]
* Update ghost to 5.110.1
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.110.1)

[4.108.2]
* Update ghost to 5.110.2
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.110.2)
* Fixed comment reference snippet overflowing ([#&#8203;22275](https://github.com/tryghost/ghost/issues/22275)) - Sanne de Vries
* Added translation logic for "Add a personal note" in Tips & Donations - Cathy Sarisky

[4.108.3]
* Update ghost to 5.110.3
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.110.3)
* Fixed email domain blocklist not being checked when a member updates their email address ([#&#8203;22320](https://github.com/tryghost/ghost/issues/22320)) - Ronald Langeveld

[4.109.0]
* Update to base image 5.0.0

[4.109.1]
* Update ghost to 5.110.4
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.110.4)
* Fixed Portal not firing member-error DOM updates ([#&#8203;22382](https://github.com/tryghost/ghost/issues/22382)) - Ronald Langeveld

[4.110.0]
* pin node to 20

[4.111.0]
* Update ghost to 5.111.0
* [Full Changelog](https://github.com/tryghost/ghost/releases/tag/v5.111.0)
* Fixed Portal not firing member-error DOM updates ([#&#8203;22382](https://github.com/tryghost/ghost/issues/22382)) - Ronald Langeveld
* Fixed email domain blocklist not being checked when a member updates their email address ([#&#8203;22320](https://github.com/tryghost/ghost/issues/22320)) - Ronald Langeveld
* Updated German translations ([#&#8203;22241](https://github.com/tryghost/ghost/issues/22241)) - Jan-T. Brinkmann
* Update Turkish translation in portal.json ([#&#8203;22308](https://github.com/tryghost/ghost/issues/22308)) - echobilisim3421

