FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

ENV NPM_CONFIG_LOGLEVEL warn
ENV NODE_ENV production

# the CLI tool uses this directory for logs
RUN mkdir /run/ghost && ln -s /run/ghost /home/cloudron/.ghost
RUN chmod -R a+r /home/cloudron/

# renovate: datasource=npm depName=ghost-cli versioning=semver
ARG GHOST_CLI_VERSION=1.27.0

# https://github.com/TryGhost/Ghost/blob/main/.docker/Dockerfile#L1
ARG NODE_VERSION=20.18.3
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

RUN npm install -g ghost-cli@${GHOST_CLI_VERSION} json

USER cloudron

# renovate: datasource=github-releases depName=tryghost/ghost versioning=semver
ENV GHOST_VERSION=5.111.0

# Despite the env var, ghost cli asks for confirmation (https://github.com/TryGhost/Ghost-CLI/blob/ff5e4ad9c30e75a10134741f96de31642123bce8/lib/utils/update-check.js#L23)
RUN echo "yes" | NO_UPDATE_NOTIFIER=1 ghost install ${GHOST_VERSION} --no-prompt --no-stack --no-setup --no-start --dbhost notlocalhost --dir /home/cloudron/ghost
RUN ln -s /app/data/config.production.json /home/cloudron/ghost/config.production.json

# the bookmark cards feature requires writing into this location - https://github.com/TryGhost/Ghost/blob/main/core/frontend/services/card-assets/service.js
RUN mv /home/cloudron/ghost/versions/${GHOST_VERSION}/core/frontend/public /home/cloudron/public_template && \
    ln -s /run/ghost/frontend_public /home/cloudron/ghost/versions/${GHOST_VERSION}/core/frontend/public

COPY config.production.json.template /home/cloudron/config.production.json.template
COPY env.template /home/cloudron/env.template
COPY start.sh /home/cloudron/start.sh

USER root

CMD [ "/home/cloudron/start.sh" ]
