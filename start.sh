#!/bin/bash

set -eu

echo "==> Start ghost"

mkdir -p /run/ghost/logs

if [[ ! -d "/app/data/content" ]]; then
    echo "==> Copying files on first run (apps, data, images and themes)"
    cp -r /home/cloudron/ghost/current/content/. /app/data/content
else
    echo "==> Clear potential migration lock"
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "UPDATE migrations_lock set locked=0 where lock_key='km01';" || true
fi

rm -rf /app/data/content/logs && ln -sf /run/ghost/logs /app/data/content/logs

cd /home/cloudron/ghost

# https://docs.ghost.org/docs/config
if [[ ! -f /app/data/config.production.json ]]; then
    cp /home/cloudron/config.production.json.template /app/data/config.production.json
fi

# 'url' is the public blog URL and 'admin' is the admin URL. we make url follow admin, if they are the same
if [[ "$(json -f /app/data/config.production.json admin)" == "$(json -f /app/data/config.production.json url)" ]]; then
    json -I -f /app/data/config.production.json -e "this.url = '${CLOUDRON_APP_ORIGIN}'"
fi

json -I -f /app/data/config.production.json \
    -e "this.admin = '${CLOUDRON_APP_ORIGIN}'" \
    -e "this.database.connection.host = '${CLOUDRON_MYSQL_HOST}'" \
    -e "this.database.connection.user = '${CLOUDRON_MYSQL_USERNAME}'" \
    -e "this.database.connection.port = '${CLOUDRON_MYSQL_PORT}'" \
    -e "this.database.connection.password = '${CLOUDRON_MYSQL_PASSWORD}'" \
    -e "this.database.connection.database = '${CLOUDRON_MYSQL_DATABASE}'"

if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
    json -I -f /app/data/config.production.json \
        -e "this.mail.from = \"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Ghost} <${CLOUDRON_MAIL_FROM}>\"" \
        -e "this.mail.options.host = '${CLOUDRON_MAIL_SMTP_SERVER}'" \
        -e "this.mail.options.port = '${CLOUDRON_MAIL_SMTP_PORT}'" \
        -e "this.mail.options.auth.user = '${CLOUDRON_MAIL_SMTP_USERNAME}'" \
        -e "this.mail.options.auth.pass = '${CLOUDRON_MAIL_SMTP_PASSWORD}'"
fi

echo "===> Copy frotend/public folder for asset generation"
cp -rf /home/cloudron/public_template /run/ghost/frontend_public

echo "==> Loading /app/data/env for potential overrides"
if [[ ! -f /app/data/env ]]; then
    cp /home/cloudron/env.template /app/data/env
fi
source /app/data/env

echo "==> Ensure permissions"
chown -R cloudron:cloudron /app/data /run/ghost

# https://docs.ghost.org/docs/cli-setup
echo "==> Migrating database"
# run knex-migrator without the --init argument to upgrade (https://forum.ghost.org/t/503-error-after-upgrade-from-2-15-0-to-2-18-1/6270)
/usr/local/bin/gosu cloudron:cloudron /home/cloudron/ghost/current/node_modules/.bin/knex-migrator init --mgpath versions/$GHOST_VERSION
/usr/local/bin/gosu cloudron:cloudron /home/cloudron/ghost/current/node_modules/.bin/knex-migrator migrate --mgpath versions/$GHOST_VERSION

# Despite the env var, ghost cli asks for confirmation (https://github.com/TryGhost/Ghost-CLI/blob/ff5e4ad9c30e75a10134741f96de31642123bce8/lib/utils/update-check.js#L23)
# echo "yes" | NO_UPDATE_NOTIFIER=1 /usr/local/bin/gosu cloudron:cloudron ghost setup migrate

echo "==> Starting Ghost"
exec /usr/local/bin/gosu cloudron:cloudron node current/index.js

