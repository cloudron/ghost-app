On first visit, you can set up an admin user and invite other users.

The admin page of Ghost is located at the $CLOUDRON-APP-ORIGIN/ghost .

Complete the [email setup](https://docs.cloudron.io/apps/ghost/#email) to configure Ghost for sending transactional and bulk emails.

