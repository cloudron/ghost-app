#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const email = 'admin@cloudron.local';
    const ghostPassword = password + '!@#$^#^*$$vsgdrt'; // ghost wants ten char password

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('gh-navigation'));
    }

    async function setup() {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn);
        await browser.executeScript('localStorage.clear();');

        await browser.get('https://' + app.fqdn + '/ghost/');

        await waitForElement(By.xpath('//input[@type="email"]'));
        await browser.findElement(By.xpath('//input[@name="blog-title"]')).sendKeys('Cloudron Ghost Blog');
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@name="name"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(ghostPassword);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await waitForElement(By.id('ob-skip'));
        await browser.findElement(By.id('ob-skip')).click();
        await waitForElement(By.xpath('//h4[text()="Welcome to your Dashboard"]'));
    }

    async function login() {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn);
        await browser.executeScript('localStorage.clear();');

        await browser.get('https://' + app.fqdn + '/ghost/signin/');

        await waitForElement(By.xpath('//input[@name="identification"]'));
        await browser.findElement(By.xpath('//input[@name="identification"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(ghostPassword);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//a[contains(text(), "Dashboard")]'));
    }

    async function checkPost() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//*[contains(text(), "Coming soon")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can get the main page', getMainPage);

    it('can setup', setup);
    xit('can login', login);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('has existing post', checkPost);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('has existing post', checkPost);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('has existing blog', checkPost);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id org.ghost.cloudronapp2 --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can setup', setup);
    xit('can login', login);
    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('check post', checkPost);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

